#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

#define MAX_POINTS 2000

typedef struct vec2_s {
    double x, y;
} vec2_type;

double vec2_dist_sq (vec2_type u, vec2_type v) {
    double diff_x = u.x - v.x;
    double diff_y = u.y - v.y;
    return diff_x*diff_x + diff_y*diff_y;
}

typedef struct vec2set_s {
    vec2_type v[MAX_POINTS];
    int size;
} vec2set_type;

void vec2set_read_file (vec2set_type* dataset, char* filename) {
    vec2_type u;
    dataset->size = 0;
    FILE* file_ptr;
    file_ptr = fopen(filename,"r");
    if (file_ptr == NULL) {
        printf ("error : could not open file %s for reading\n",filename);
        exit(1);
    }
    while (fscanf (file_ptr,"%lf %lf",&(u.x),&(u.y)) == 2) {
        if (dataset->size < MAX_POINTS) {
            dataset->v[dataset->size] = u;
            dataset->size += 1;
        } else {
            printf ("Too many points in file %s\n",filename);
            fclose (file_ptr);
            exit(1);
        }
    }
    fclose (file_ptr);
}

/* calculate the cost squared of a given set of center locations */
double center_cost_sq (vec2set_type* dataset, int* centers, int k) {
    double cost_sq = 0;
    for (int i=0;i<dataset->size;i++) {
        double min_dist_sq = DBL_MAX;
        for (int j=0;j<k;j++) {
            double dist_sq = vec2_dist_sq(dataset->v[i],dataset->v[centers[j]]);
            if (dist_sq < min_dist_sq) {
                min_dist_sq = dist_sq;
            }
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
        }
    }
    return cost_sq;
}

int main (int argc, char** argv) {

    /* get filename, k, m, and s from command line */
    if (argc < 5) {
        printf ("Command usage : %s %s %s %s %s\n",argv[0],"filename","k","m","s");
        return 1;
    }

    vec2set_type dataset;
    vec2set_read_file (&dataset,argv[1]);
    int k = atoi(argv[2]);
    int m = atoi(argv[3]);

    /* seed the random number generator */
    srandom(atoi(argv[4]));

    /* check the cost_sq of m random sets of k centers */
    int centers[k];
    int optimal_centers[k];
    double optimal_cost_sq = DBL_MAX;
    double cost_sq;
    for (int i=0;i<m;i++) {
        for (int j=0;j<k;j++) {
            centers[j] = random() % dataset.size;
        }
        cost_sq = center_cost_sq(&dataset,centers,k);
        if (cost_sq < optimal_cost_sq) {
            optimal_cost_sq = cost_sq;
            for (int j=0;j<k;j++) {
                optimal_centers[j] = centers[j];
            }
        }
    }

    /* print out the total number of k-tuples checked */
    printf ("total number of %d-tuples checked = %d\n",k,m);

    /* print the approximate optimal cost for the k-center problem */
    printf ("approximate optimal cost = %g\n",sqrt(optimal_cost_sq));

    /* print an approx optimal solution to the k-center problem */
    printf ("approx optimal centers : ");
    for (int j=0;j<k;j++) {
        printf ("%d ",optimal_centers[j]);
    }
    printf ("\n");

}
