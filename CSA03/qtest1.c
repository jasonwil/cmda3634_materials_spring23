#include <stdio.h>
#include "queue.h"

int main () {
    queue_type queue;
    queue_init (&queue);

    for (int i=0;i<MAX_QUEUE_SIZE+1;i++) {
	queue_insert_back(&queue,i);
    }
}
