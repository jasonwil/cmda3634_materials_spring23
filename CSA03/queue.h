#ifndef QUEUE_H
#define QUEUE_H

#define MAX_QUEUE_SIZE 100000

typedef struct queue_s {
    int data[MAX_QUEUE_SIZE];
    int size;
    int front;
    int back;
} queue_type;

/* initializes a queue to be empty */
void queue_init (queue_type* queue);

/* returns the front element of the queue */
/* exits with an error if the queue is empty */
int queue_front (queue_type* queue);

/* inserts given element at the back of the queue */
/* exits with an error if the queue is full */
void queue_insert_back (queue_type* queue, int element);

/* removes the front element of the queue */
/* exits with an error if the queue is empty */
void queue_remove_front (queue_type* queue);

#endif
