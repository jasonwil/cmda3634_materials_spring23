#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

int main () {
    queue_type queue;
    queue_init (&queue);

    /* part 1 */
    for (int i=0;i<MAX_QUEUE_SIZE;i++) {
	queue_insert_back(&queue,i);
    }
    for (int i=0;i<MAX_QUEUE_SIZE/2;i++) {
	int front = queue_front(&queue);
	if (front != i) {
	    printf ("Part 1 of qtest4 failed!\n");
	    exit(1);
	}
	queue_remove_front(&queue);
    }
    /* part 2 */
    for (int i=0;i<MAX_QUEUE_SIZE/2;i++) {
	queue_insert_back(&queue,i);
    }
    for (int i=0;i<MAX_QUEUE_SIZE/2;i++) {
	int front = queue_front(&queue);
	if (front != i+MAX_QUEUE_SIZE/2) {
	    printf ("Part 2 of qtest4 failed!\n");
	    exit(1);
	}
	queue_remove_front(&queue);
    }
    /* part 3 */
    for (int i=0;i<MAX_QUEUE_SIZE/2;i++) {
	int front = queue_front(&queue);
	if (front != i) {
	    printf ("Part 3 of qtest4 failed!\n");
	    exit(1);
	}
	queue_remove_front(&queue);
    }
    /* part 4 */
    for (int i=1;i<=1000;i++) {
	for (int j=0;j<i;j++) {
	    queue_insert_back(&queue,i+j);
	}
	for (int j=0;j<i;j++) {
	    int front = queue_front(&queue);
	    if (front != i+j) {
		printf ("Part 4 of qtest4 failed!\n");		
	    }
	    queue_remove_front(&queue);
	}
    }
    printf ("qtest4 passed!\n");
}
