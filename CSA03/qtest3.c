#include <stdio.h>
#include "queue.h"

int main () {
    queue_type queue;
    queue_init (&queue);

    for (int i=0;i<MAX_QUEUE_SIZE;i++) {
	queue_insert_back(&queue,i);
    }
    for (int i=0;i<MAX_QUEUE_SIZE;i++) {
	queue_remove_front(&queue);
    }
    printf ("The front element is %d\n",queue_front(&queue));
}
