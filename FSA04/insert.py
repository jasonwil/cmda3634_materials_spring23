# read the numbers into a python array
n = int(input()[1:])
numbers = [0]*n
for i in range(n):
    numbers[i] = int(input())

# Insertion sort 
for i in range(n):
    cur = i
    done = False
    next = numbers[i]
    while (cur > 0 and not done):
        if (numbers[cur-1] > next):
            numbers[cur] = numbers[cur-1]
            cur -= 1
        else:
            done = True
    numbers[cur] = next

# print the result one number per line
print ("# "+str(n))
for i in numbers:
    print (i)
