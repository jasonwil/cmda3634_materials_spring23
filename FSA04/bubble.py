# read the numbers into a python array
n = int(input()[1:])
numbers = [0]*n
for i in range(n):
    numbers[i] = int(input())

# Bubble sort
done = False
while (done == False):
    done = True
    for i in range(n-1):
        if (numbers[i] > numbers[i+1]):
            temp = numbers[i]
            numbers[i] = numbers[i+1]
            numbers[i+1] = temp
            done = False

# print the result one number per line
print ("# "+str(n))
for i in numbers:
    print (i)
