#include <stdio.h>
#include <stdlib.h>

int main () {
    /* read the numbers into a C array */
    int n;
    scanf ("%*c %d",&n);
    int* numbers = calloc(n,sizeof(int));
    for (int i=0;i<n;i++) {
	scanf ("%d",&(numbers[i]));
    }

    /* Bubble sort */
    int done = 0;
    while (!done) {
	done = 1;
	for (int i=0;i<n-1;i++) {
	    if (numbers[i] > numbers[i+1]) {
		int temp = numbers[i];
		numbers[i] = numbers[i+1];
		numbers[i+1] = temp;
		done = 0;
	    }
	}
    }

    /* print the result one number per line */
    printf ("# %d\n",n);
    for (int i=0;i<n;i++) {
	printf ("%d\n",numbers[i]);
    }

    /* free the dynamically allocated numbers array and return */
    free (numbers);
    return 0;
}
