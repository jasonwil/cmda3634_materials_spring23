#include <stdio.h>
#include <stdlib.h>

int main () {
    /* read the numbers into a C array */
    int n;
    scanf ("%*c %d",&n);
    int* numbers = calloc(n,sizeof(int));
    for (int i=0;i<n;i++) {
	scanf ("%d",&(numbers[i]));
    }

    /* Insertion sort */
    for (int i=0;i<n;i++) {
	int cur = i;
	int done = 0;
	int next = numbers[i];
	while (cur > 0 && !done) {
	    if (numbers[cur-1] > next) {
		numbers[cur] = numbers[cur-1];
		cur -= 1;
	    } else {
		done = 1;
	    }
	}
	numbers[cur] = next;
    }

    /* print the result one number per line */
    printf ("# %d\n",n);
    for (int i=0;i<n;i++) {
	printf ("%d\n",numbers[i]);
    }

    /* free the dynamically allocated numbers array and return */
    free (numbers);
    return 0;
}
