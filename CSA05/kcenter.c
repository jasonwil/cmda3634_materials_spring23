#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

/* compute the distance squared between two points in 2d */
float distance_sq(float* p1, float* p2) {
    /* add code here */
}

/* copy the length k array src to the length k array dest */
void array_copyto(int k, int* dest, int* src) {
    /* add code here */
}

/* compute the cost squared given k centers */
/* n is the number of points and k is the number of centers */
float centers_cost_sq(int n, int k, float* points, int* centers) {
    /* add code here */
}

/* approximates a solution to the k-center problem on n points in 2d */
/* k is the number of centers, m is the number of guesses */
/* returns the approximate minimal cost and indices of the corresponding centers */
float approx_kcenter(int n, int k, int m, float* points, int* guesses, int* centers) {
    /* add code here */
}
