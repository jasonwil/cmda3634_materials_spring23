import sys
import numpy as np
import matplotlib.pyplot as plt

# read the data file containing 2d points
points = np.loadtxt(sys.argv[1],dtype=np.float32)

# compute the distance squared between two points
def distance_sq(p1,p2):
    return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])

# compute the cost squared given k centers
def centers_cost_sq(points,centers):
    cost_sq = 0
    n = len(points)
    k = len(centers)
    for i in range(n):
        min_dist_sq = float("inf")
        for j in range(k):
            dist_sq = distance_sq(points[i],points[centers[j]])
            if (dist_sq < min_dist_sq):
                min_dist_sq = dist_sq
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
    return cost_sq

# approximate a solution to the k-center problem
def approx_kcenter(points,guesses,centers):
    min_cost_sq = float("inf")
    n = len(points)
    m = len(guesses)
    k = len(centers)
    for i in range(m):
        cost_sq = centers_cost_sq(points,guesses[i])
        if (cost_sq < min_cost_sq):
            min_cost_sq = cost_sq
            np.copyto(centers,guesses[i])
    return np.sqrt(min_cost_sq)

# Approximate the k-center problem using pure Python
k = int(sys.argv[2])
m = int(sys.argv[3])
seed = int(sys.argv[4])
centers = np.zeros(k,dtype='int32')
n = len(points)
np.random.seed(seed)
guesses = np.random.rand(m,n).argpartition(k,axis=1)[:,:k].astype(np.int32)
min_cost = approx_kcenter(points,guesses,centers)
print ('approximate kcenter solution is',centers)
print ('approximate minimal cost is',np.round(min_cost,5))

# compute the clusters and extreme point given k centers
def centers_clusters_extreme(points,centers,clusters):
    cost_sq = 0
    n = len(points)
    k = len(centers)
    for i in range(n):
        min_dist_sq = float("inf")
        for j in range(k):
            dist_sq = distance_sq(points[i],points[centers[j]])
            if (dist_sq < min_dist_sq):
                min_dist_sq = dist_sq
                clusters[i] = j
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
            extreme = i
    return extreme

# plot the points showing centers, clusters, and the extreme point
if (len(sys.argv) > 5):
    clusters = np.zeros(len(points),dtype='int32')
    extreme = centers_clusters_extreme(points,centers,clusters)
    plt.gca().set_aspect('equal')
    plt.scatter (points[:,0],points[:,1],c=clusters,cmap="tab10",s=10)
    plt.scatter (points[centers,0],points[centers,1],c=range(k),cmap="tab10",s=100)
    plt.scatter(points[extreme,0],points[extreme,1],
            s=50,facecolors='none', edgecolors='black')
    plt.savefig(sys.argv[5])
