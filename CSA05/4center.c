#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

/* compute the distance squared between two points in 2d */
float distance_sq(float* p1, float* p2) {
    return ((p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1]));
}

/* return the minimum of the 4 input values */
float min4(float a, float b, float c, float d) {
    float min = a;
    if (b < min) {
        min = b;
    }
    if (c < min) {
        min = c;
    }
    if (d < min) {
        min = d;
    }
    return min;
}

/* compute the cost squared given three centers */
/* n is the number of points */
float centers_cost_sq(int n, float* points, int c1, int c2, int c3, int c4) {
    float cost_sq = 0;
    for (int i=0;i<n;i++) {
        float dist_sq_1 = distance_sq(points+2*i,points+2*c1);
        float dist_sq_2 = distance_sq(points+2*i,points+2*c2);
        float dist_sq_3 = distance_sq(points+2*i,points+2*c3);
        float dist_sq_4 = distance_sq(points+2*i,points+2*c4);
        float min_dist_sq = min4(dist_sq_1,dist_sq_2,dist_sq_3,dist_sq_4);
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
        }
    }
    return cost_sq;
}

/* solve the 4-center problem on n points in 2d */
/* returns the minimal cost and indices of the corresponding centers */
float solve_4center (int n, float* points, int* centers) {
    int num_checked = 0;
    float min_cost_sq = FLT_MAX;
    for (int i=0;i<n-3;i++) {
        for (int j=i+1;j<n-2;j++) {
            for (int k=j+1;k<n-1;k++) {
                for (int l=k+1;l<n;l++) {
                    float cost_sq = centers_cost_sq(n,points,i,j,k,l);
                    num_checked += 1;
                    if (cost_sq < min_cost_sq) {
                        min_cost_sq = cost_sq;
                        centers[0] = i;
                        centers[1] = j;
                        centers[2] = k;
                        centers[3] = l;
                    }
                }
            }
        }
    }
    printf ("number of quadruples checked = %d\n",num_checked);
    return sqrt(min_cost_sq);
}
