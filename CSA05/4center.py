import sys
import numpy as np
import matplotlib.pyplot as plt

# read the data file containing 2d points
points = np.loadtxt(sys.argv[1],dtype=np.float32)

# compute the distance squared between two points
def distance_sq(p1,p2):
    return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])

# return the minimum of the 4 input values
def min4(a,b,c,d):
    min = a
    if (b < min):
        min = b
    if (c < min):
        min = c
    if (d < min):
        min = d
    return min

# compute the cost squared given four centers
def centers_cost_sq(points,c1,c2,c3,c4):
    cost_sq = 0
    n = len(points)
    for i in range(n):
        dist_sq_1 = distance_sq(points[i],points[c1])
        dist_sq_2 = distance_sq(points[i],points[c2])
        dist_sq_3 = distance_sq(points[i],points[c3])
        dist_sq_4 = distance_sq(points[i],points[c4])
        min_dist_sq = min4(dist_sq_1,dist_sq_2,dist_sq_3,dist_sq_4)
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
    return cost_sq

# solve the 4-center problem
def solve_4center(points,centers):
    num_checked = 0
    min_cost_sq = float("inf")
    n = len(points)
    for i in range(0,n-3):
        for j in range(i+1,n-2):
            for k in range(j+1,n-1):
                for l in range(k+1,n):
                    num_checked += 1
                    cost_sq = centers_cost_sq(points,i,j,k,l)
                    if (cost_sq < min_cost_sq):
                        min_cost_sq = cost_sq
                        centers[0] = i
                        centers[1] = j
                        centers[2] = k
                        centers[3] = l
    print ('number of quadruples checked =',num_checked)
    return np.sqrt(min_cost_sq)

# Solve the 4-center problem using our Python function
centers = np.zeros(4,dtype='int32')
min_cost = solve_4center(points,centers)
print ('4-center solution is',centers)
print ('minimal cost is',np.round(min_cost,5))

# compute the clusters and extreme point given three centers
def centers_clusters_extreme(points,centers,clusters):
    cost_sq = 0
    n = len(points)
    for i in range(n):
        min_dist_sq = float("inf")
        for j in range(4):
            dist_sq = distance_sq(points[i],points[centers[j]])
            if (dist_sq < min_dist_sq):
                clusters[i] = j
                min_dist_sq = dist_sq
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
            extreme = i
    return extreme

# plot the points showing centers, clusters, and the extreme point
if (len(sys.argv) > 2):
    clusters = np.zeros(len(points),dtype='int32')
    extreme = centers_clusters_extreme(points,centers,clusters)
    plt.gca().set_aspect('equal')
    plt.scatter (points[:,0],points[:,1],c=clusters,cmap="tab10",s=10)
    plt.scatter (points[centers,0],points[centers,1],c=range(4),cmap="tab10",s=100)
    plt.scatter(points[extreme,0],points[extreme,1],
            s=50,facecolors='none', edgecolors='black')
    plt.savefig(sys.argv[2])
