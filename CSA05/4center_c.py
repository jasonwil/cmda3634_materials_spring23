import sys
import numpy as np
import matplotlib.pyplot as plt

# Load our C function for solving the 4-center problem
from ctypes import c_int, c_float, c_void_p, cdll
lib = cdll.LoadLibrary("./4center.so")
solve_4center_c = lib.solve_4center
solve_4center_c.restype = c_float

# read the data file containing 2d points
points = np.loadtxt(sys.argv[1],dtype=np.float32)

# Solve the 4-center problem using our Python function
centers = np.zeros(4,dtype='int32')
min_cost = solve_4center_c(c_int(len(points)),
        c_void_p(points.ctypes.data),
        c_void_p(centers.ctypes.data))
print ('4-center solution is',centers)
print ('minimal cost is',np.round(min_cost,5))

# compute the distance squared between two points
def distance_sq(p1,p2):
    return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])

# compute the clusters and extreme point given three centers
def centers_clusters_extreme(points,centers,clusters):
    cost_sq = 0
    n = len(points)
    for i in range(n):
        min_dist_sq = float("inf")
        for j in range(4):
            dist_sq = distance_sq(points[i],points[centers[j]])
            if (dist_sq < min_dist_sq):
                clusters[i] = j
                min_dist_sq = dist_sq
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
            extreme = i
    return extreme

# plot the points showing centers, clusters, and the extreme point
if (len(sys.argv) > 2):
    clusters = np.zeros(len(points),dtype='int32')
    extreme = centers_clusters_extreme(points,centers,clusters)
    plt.gca().set_aspect('equal')
    plt.scatter (points[:,0],points[:,1],c=clusters,cmap="tab10",s=10)
    plt.scatter (points[centers,0],points[centers,1],c=range(4),cmap="tab10",s=100)
    plt.scatter(points[extreme,0],points[extreme,1],
            s=50,facecolors='none', edgecolors='black')
    plt.savefig(sys.argv[2])
