#ifndef MAXHEAP_H
#define MAXHEAP_H 1

#define MAX_MAXHEAP_SIZE 100000

/* heap element type */
typedef struct heap_element_s {
    double value;
    int index;
} heap_element_type;

/* maxheap structure */
typedef struct maxheap_s {
    heap_element_type nodes[MAX_MAXHEAP_SIZE];
    int size;
} maxheap_type;

/* initialize heap */
void maxheap_init(maxheap_type* heap);

/* return size of heap */
int maxheap_size(maxheap_type* heap);

/* return element on top of heap */
heap_element_type maxheap_top(maxheap_type* heap);

/* insert an element into heap */
void maxheap_insert(maxheap_type* heap, heap_element_type new);

/* remove element on top of heap */
void maxheap_remove_top(maxheap_type* heap);

#endif
