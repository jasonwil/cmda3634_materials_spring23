#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <mpi.h>
#include "mat.h"

typedef struct double_int_s {
    double cost_sq;
    int arg_max;
} double_int_type;

/* calculate the center cost squared and arg max */
double center_cost_sq (mat_type* dataset, int* centers, int i, int* arg_max,
        int rank, int size) {
    double cost_sq = 0;
    for (int j=0;j<dataset->rows;j++) {
        vec_type row_j, row_center;
        double min_dist_sq = DBL_MAX;
        mat_get_row(dataset,&row_j,j);
        for (int m=0;m<i;m++) {
            mat_get_row(dataset,&row_center,centers[m]);
            double dist_sq = vec_dist_sq(&row_j,&row_center);
            if (dist_sq < min_dist_sq) {
                min_dist_sq = dist_sq;
            }
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
            *arg_max = j;
        }
    }
    return cost_sq;
}

/* find the cluster for the given point */
int find_cluster (mat_type* kmeans, vec_type* point) {
    int cluster;

    double min_dist_sq = DBL_MAX;
    vec_type kmean;
    for (int i=0;i<kmeans->rows;i++) {
        mat_get_row(kmeans,&kmean,i);
        double dist_sq = vec_dist_sq(&kmean,point);
        if (dist_sq < min_dist_sq) {
            min_dist_sq = dist_sq;
            cluster = i;
        }
    }

    return cluster;
}

/* calculate the next kmeans */
void calc_kmeans (mat_type* dataset, mat_type* kmeans, mat_type* kmeans_next,
        int rank, int size) {
    int num_points[kmeans->rows];
    for (int i=0;i<kmeans->rows;i++) {
        num_points[i] = 0;
    }
    mat_zero(kmeans_next);
    vec_type point, kmean;
    int cluster;
    for (int i=0;i<dataset->rows;i++) {
        mat_get_row(dataset,&point,i);
        cluster = find_cluster(kmeans,&point);
        mat_get_row(kmeans_next,&kmean,cluster);
        vec_add(&kmean,&point,&kmean);
        num_points[cluster] += 1;
    }
    for (int i=0;i<kmeans_next->rows;i++) {
        mat_get_row(kmeans_next,&kmean,i);	
        if (num_points[i] > 0) {
            vec_scalar_mult(&kmean,1.0/num_points[i],&kmean);
        } else {
            printf ("error : cluster has no points!\n");
            exit(1);
        }
    }
}

int main (int argc, char** argv) {

    /* initialize MPI */
    MPI_Init (&argc, &argv);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* get k and m from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"k","m");
        return 1;
    }
    int k = atoi(argv[1]);
    int m = atoi(argv[2]);

    /* read in the mnist training set of 60000 images */
    int rows = 60000;
    int cols = 784;

    /* dynamically allocate memory for the (rows x cols) data matrix */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix and read the binary file */
    mat_type dataset;
    mat_init (&dataset,data,rows,cols);
    matrix_read_bin(&dataset,"train-images-idx3-ubyte",16);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    /* find k centers using the farthest first algorithm */
    int centers[k];
    double cost_sq;
    int arg_max;
    centers[0] = 0;
    for (int i=1;i<k;i++) {
        cost_sq = center_cost_sq(&dataset,centers,i,&arg_max,rank,size);
        centers[i] = arg_max;
    }

    /* initialize kmeans using the k centers */
    double kmeans_data[k*cols];
    mat_type kmeans;
    mat_init(&kmeans,kmeans_data,k,cols);
    for (int i=0;i<k;i++) {
        vec_type center, kmean;
        mat_get_row (&dataset,&center,centers[i]);
        mat_get_row (&kmeans,&kmean,i);
        vec_copy (&kmean,&center);
    }

    /* update kmeans m times */
    double kmeans_next_data[k*cols];
    mat_type kmeans_next;
    mat_init(&kmeans_next,kmeans_next_data,k,cols);
    for (int iter=0;iter<m;iter++) {
        calc_kmeans (&dataset,&kmeans,&kmeans_next,rank,size);
        mat_copy (&kmeans,&kmeans_next);
    }

    /* stop the timer */
    end_time = MPI_Wtime();

#ifdef TIMING
    printf ("(%d,%.4f),",size,(end_time-start_time));
#else
    /* print out wall time used */
    printf ("# wall time used = %g sec\n",end_time-start_time);

    /* print the results */
    mat_print(&kmeans,"%g ");
#endif

    /* free the dynamically allocated matrix data buffer */
    free(data);

    /* Finalize MPI */
    MPI_Finalize();
}
