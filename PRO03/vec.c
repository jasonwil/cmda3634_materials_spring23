#include <stdio.h>
#include <stdlib.h>
#include "vec.h"

/* initialize a vector to use a given data buffer of size dim */
/* vec_init must be called before any other vector operation */
void vec_init (vec_type* v, double* data, int dim) {
    v->data = data;
    v->dim = dim;
}

/* print a vector using the provided format string */
void vec_print (vec_type* v, char* format) {
    for (int i=0;i<v->dim;i++) {
	printf (format,v->data[i]);
    }
    printf ("\n");
}

/* w = u + v */
void vec_add (vec_type* u, vec_type* v, vec_type* w) {
    for (int i=0;i<v->dim;i++) {
	w->data[i] = u->data[i] + v->data[i];
    }
}

/* w = cv */
void vec_scalar_mult (vec_type* v, double c, vec_type* w) {
    for (int i=0;i<v->dim;i++) {
	w->data[i] = v->data[i]*c;
    }
}

/* reads a vector from STDIN */
/* returns how many elements read */
int vec_read_stdin (vec_type* v) {
    int num_read = 0;
    for (int i=0;i<v->dim;i++) {
	if (scanf("%lf",&(v->data[i])) == 1) {
	    num_read += 1;
	}
    }
    return num_read;
}

/* returns || v - w ||^2 */
double vec_dist_sq (vec_type* v, vec_type* w) {
    double sum_sq = 0;
    for (int i=0;i<v->dim;i++) {
	double diff = v->data[i] - w->data[i];
	sum_sq += diff*diff;
    }
    return sum_sq;
}

/* performs the deep copy v->data[i] = w->data[i] for all i */
void vec_copy (vec_type* v, vec_type* w) {
    for (int i=0;i<v->dim;i++) {
	v->data[i] = w->data[i];
    }
}

/* zeros the vector v */
void vec_zero (vec_type* v) {
    for (int i=0;i<v->dim;i++) {
	v->data[i] = 0;
    }
}
