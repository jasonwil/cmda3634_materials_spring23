#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=8
#SBATCH -o mpi_kmeans_timing.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# Build the executable
mpicc -D TIMING -o mpi_kmeans mpi_kmeans.c mat.c vec.c

# run mpi_kmeans
mpiexec -n 1 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_kmeans $1 $2
mpiexec -n 2 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_kmeans $1 $2
mpiexec -n 4 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_kmeans $1 $2
mpiexec -n 8 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_kmeans $1 $2
mpiexec -n 16 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_kmeans $1 $2
