import sys
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps # for image processing

if (len(sys.argv) < 3):
    print ("Not enough command line arguments")
    quit()

infile = sys.argv[1]
outfile = sys.argv[2]

# read the kmeans colors
data = np.loadtxt(sys.stdin)
data = np.round(data)
data = data.astype(int)
k = len(data)

# replace each colors with the nearest mean color 
im = Image.open(infile)
A = np.array(im)
rows,cols,colors = A.shape
my_y_kmeans = np.zeros((rows,cols))
dist_sq = np.zeros((rows,cols,k))
for i in range(k):
    dist_sq[:,:,i] = np.sum((A-data[i])*(A-data[i]),axis=2)
my_y_kmeans = np.argmin(dist_sq,axis=2)
A = data[my_y_kmeans]

# the resulting image 
imk = Image.fromarray(np.uint8(A))
imk.save (outfile)
