#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "mat.h"

/* calculate the center cost squared and arg max */
double center_cost_sq (mat_type* dataset, int* centers, int i, int* arg_max) {
    double cost_sq = 0;
    for (int j=0;j<dataset->rows;j++) {
	vec_type row_j, row_center;
	double min_dist_sq = DBL_MAX;
	mat_get_row(dataset,&row_j,j);
	for (int m=0;m<i;m++) {
	    mat_get_row(dataset,&row_center,centers[m]);
	    double dist_sq = vec_dist_sq(&row_j,&row_center);
	    if (dist_sq < min_dist_sq) {
		min_dist_sq = dist_sq;
	    }
	}
	if (min_dist_sq > cost_sq) {
	    cost_sq = min_dist_sq;
	    *arg_max = j;
	}
    }
    return cost_sq;
}

/* find the cluster for the given point */
int find_cluster (mat_type* kmeans, vec_type* point) {
    int cluster;

    /*****************/
    /* add code here */
    /*****************/

    return cluster;
}

/* calculate the next kmeans */
void calc_kmeans (mat_type* dataset, mat_type* kmeans, mat_type* kmeans_next) {

    /*****************/
    /* add code here */
    /*****************/

}

int main (int argc, char** argv) {

    /* get k and m from the command line */
    if (argc < 3) {
	printf ("Command usage : %s %s %s\n",argv[0],"k","m");
	return 1;
    }
    int k = atoi(argv[1]);
    int m = atoi(argv[2]);

    /* read the shape of the data matrix from STDIN */
    int rows, cols;
    if (scanf("%d %d",&rows,&cols) != 2) {
	printf ("error reading the shape of the matrix\n");
	return 1;
    }

    /* dynamically allocate memory for the (rows x cols) data matrix */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix */
    mat_type dataset;
    mat_init (&dataset,data,rows,cols);

    /* read the data matrix from STDIN */
    int num_read = mat_read_stdin (&dataset);

    /* find k centers using the farthest first algorithm */
    int centers[k];
    double cost_sq;
    int arg_max;
    centers[0] = 0;
    for (int i=1;i<k;i++) {
	cost_sq = center_cost_sq(&dataset,centers,i,&arg_max);
	centers[i] = arg_max;
    }

    /* initialize kmeans using the k centers */
    double kmeans_data[k*cols];
    mat_type kmeans;
    mat_init(&kmeans,kmeans_data,k,cols);
    for (int i=0;i<k;i++) {
	vec_type center, kmean;
	mat_get_row (&dataset,&center,centers[i]);
	mat_get_row (&kmeans,&kmean,i);
	vec_copy (&kmean,&center);
    }

    /* update kmeans m times */
    double kmeans_next_data[k*cols];
    mat_type kmeans_next;
    mat_init(&kmeans_next,kmeans_next_data,k,cols);
    for (int iter=0;iter<m;iter++) {
        calc_kmeans (&dataset, &kmeans, &kmeans_next);
	mat_copy (&kmeans,&kmeans_next);
    }

    /* print the results */
    mat_print(&kmeans,"%g ");

    /* free the dynamically allocated matrix data buffer */
    free(data);

    return 0;

}
