import sys
from PIL import Image, ImageOps # for image processing
import numpy as np # for matrix processing

if (len(sys.argv) < 2):
    print ("Not enough command line arguments")
    quit()

infile = sys.argv[1]

im = Image.open(infile)
A = np.array(im)
rows,cols,colors = A.shape
print (rows*cols,colors)
np.savetxt(sys.stdout,A.reshape(rows*cols,colors),fmt='%d')
        
