#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --cpus-per-task=16
#SBATCH -o omp_farfirst_ec_timing.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# Build the executable
gcc -D TIMING -o omp_farfirst_ec omp_farfirst_ec.c mat.c vec.c -fopenmp -lm

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=spread
export OMP_PLACES=cores

# run omp_farfirst
cat $1 | ./omp_farfirst_ec $2 1
cat $1 | ./omp_farfirst_ec $2 2
cat $1 | ./omp_farfirst_ec $2 4
cat $1 | ./omp_farfirst_ec $2 8
cat $1 | ./omp_farfirst_ec $2 16
