#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <omp.h>
#include "mat.h"

/* calculate the center cost squared and arg max */
double center_cost_sq (mat_type* dataset, int* centers, int i, int* arg_max, vec_type* dists_sq) {
    double cost_sq = 0;
    vec_type row_j, row_center;
    mat_get_row(dataset,&row_center,centers[i-1]);
    for (int j=0;j<dataset->rows;j++) {
        mat_get_row(dataset,&row_j,j);
        double dist_sq = vec_dist_sq(&row_j,&row_center);
        if (dist_sq < dists_sq->data[j]) {
            dists_sq->data[j] = dist_sq;
        }
        if (dists_sq->data[j] > cost_sq) {
            cost_sq = dists_sq->data[j];
            *arg_max = j;
        }
    }
    return cost_sq;
}

int main (int argc, char** argv) {

    /* get k and thread_count from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s\n",argv[0],"k","thread_count");
        return 1;
    }
    int k = atoi(argv[1]);
    int thread_count = atoi(argv[2]);
    omp_set_num_threads(thread_count);

    /* print out the thread count */
#ifndef TIMING
    printf ("# thread_count = %d\n",thread_count);
#endif

    /* read the shape of the matrix from STDIN */
    int rows, cols;
    if (scanf("%d %d",&rows,&cols) != 2) {
        printf ("error reading the shape of the matrix\n");
        return 1;
    }

    /* dynamically allocate memory for the (rows x cols) matrix */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix */
    mat_type dataset;
    mat_init (&dataset,data,rows,cols);

    /* read the matrix from STDIN */
    int num_read = mat_read_stdin (&dataset);

    /* dynamically allocate memory for the dists_sq vector */
    double* dists_sq_data = (double*)malloc(rows*sizeof(double));

    /* initialize the dists_sq vector */
    vec_type dists_sq;
    vec_init (&dists_sq,dists_sq_data,rows);
    for (int i=0;i<rows;i++) {
        dists_sq.data[i] = DBL_MAX;
    }

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* find k centers using the farthest first algorithm */
    int centers[k];
    double cost_sq;
    int arg_max;
    centers[0] = 0;
    for (int i=1;i<k;i++) {
        cost_sq = center_cost_sq(&dataset,centers,i,&arg_max,&dists_sq);
        centers[i] = arg_max;
    }

    /* calculate the cost of the k centers */
    cost_sq = center_cost_sq(&dataset,centers,k,&arg_max,&dists_sq);
    double cost = sqrt(cost_sq);

    /* stop the timer */
    end_time = omp_get_wtime();

#ifdef TIMING
    printf ("(%d,%.4f),",thread_count,(end_time-start_time));
#else
    /* print out wall time used */
    printf ("# wall time used = %g sec\n",end_time-start_time);

    /* print the approximate minimal cost for the k-center problem */
    printf ("# approximate optimal cost = %g\n",cost);

    /* print an approx optimal solution to the k-center problem */
    printf ("# approx optimal centers : \n");
    for (int i=0;i<k;i++) {
        vec_type row_i;
        mat_get_row (&dataset, &row_i, centers[i]);
        vec_print (&row_i,"%g ");
    }
#endif

    /* free the dynamically allocated matrix and vector data buffers */
    free (data);
    free (dists_sq_data);
}
