#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --cpus-per-task=16
#SBATCH -o omp_kmeans_timing.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# Build the executable
gcc -D TIMING -o omp_kmeans omp_kmeans.c mat.c vec.c -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=spread
export OMP_PLACES=cores

# run omp_kmeans
cat $1 | ./omp_kmeans $2 $3 1
cat $1 | ./omp_kmeans $2 $3 2
cat $1 | ./omp_kmeans $2 $3 4
cat $1 | ./omp_kmeans $2 $3 8
cat $1 | ./omp_kmeans $2 $3 16

