import sys
import numpy as np
import matplotlib.pyplot as plt

# Load our C function 
from ctypes import c_int, c_float, c_void_p, cdll
lib = cdll.LoadLibrary("./snoopy.so")
print_points_c = lib.print_points

# read the data file containing 2d points
points = np.loadtxt(sys.argv[1],dtype=np.float32)

# call our C function to print the points
rows = len(points)
cols = len(points[0])
print_points_c(c_int(rows),c_int(cols),c_void_p(points.ctypes.data));


