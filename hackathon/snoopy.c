#include <stdio.h>

void print_row (int cols, float* row) {
    printf ("[ ");
    for (int i=0;i<cols;i++) {
	printf ("%g ",row[i]);
    }
    printf ("]\n");
}

void print_points (int rows, int cols, float* data) {
    for (int i = 0;i<rows;i++) {
	print_row (cols, data + i*cols);
    }
}
