#include <stdio.h>
#include <stdlib.h>

typedef struct centers_s {
    double cost_sq;
    int c[3];
} center_type;

optimal_centers = thread_optimal_centers;

int main () {
    center_type centers = { DBL_MAX, {-1,-1,-1} };
    center_type optimal_centers = centers;
    centers.c[1] = 2;
    printf ("%g %d %d %d\n",centers.cost_sq, centers.c[0], centers.c[1], 
	   centers.c[2]);
    printf ("%g %d %d %d\n",optimal_centers.cost_sq, optimal_centers.c[0], 
	    optimal_centers.c[1], optimal_centers.c[2]);

}
