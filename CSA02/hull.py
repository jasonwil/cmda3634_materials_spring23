import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 2):
    print ("Command Usage : python3",sys.argv[0],"outfile")
    exit(1)
outfile = sys.argv[1]

# read the data file
data = np.loadtxt("data.txt")

# read the edge list
edges = np.loadtxt(sys.stdin,dtype='int')

# plot the data
plt.gca().set_aspect('equal')
plt.scatter(data[:,0],data[:,1],s=10,color='black')

# plot the special points (if additional command line argments present)
num_edges = len(edges)
for edge in edges:
    p1 = data[edge[0]]
    p2 = data[edge[1]]
    plt.plot([p1[0],p2[0]],[p1[1],p2[1]],color='red')

# save the plot as an image
plt.savefig(outfile)

