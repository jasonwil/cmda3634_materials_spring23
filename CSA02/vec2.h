#ifndef VEC2_H
#define VEC2_H

typedef struct vec2_s {
    double x, y;
} vec2_type;

#endif
