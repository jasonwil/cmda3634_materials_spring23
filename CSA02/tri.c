#include <stdio.h>
#include "data.h"

/* returns 1 if triangle is in Delaunay triangulation and 0 otherwise */
int test_triangle (int i, int j, int k) {
    /* test to see if the triangle with vertices data[i], data[j], data[k] */
    /* is in the Delaunay triangulation or not */
}

int main () {
    /* test all triangles in the dataset (only once!) using test_triangle */
    /* print only the triangles in the Delaunay triangulation */
}
