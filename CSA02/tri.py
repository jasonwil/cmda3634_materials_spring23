import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 2):
    print ("Command Usage : python3",sys.argv[0],"outfile")
    exit(1)
outfile = sys.argv[1]

# read the data file
data = np.loadtxt("data.txt")

# read the triangle list
triangles = np.loadtxt(sys.stdin,dtype='int')

# plot the data
plt.gca().set_aspect('equal')
plt.scatter(data[:,0],data[:,1],s=10,color='black')

# plot the special points (if additional command line argments present)
for triangle in triangles:
    p1 = data[triangle[0]]
    p2 = data[triangle[1]]
    p3 = data[triangle[2]]
    plt.plot([p1[0],p2[0],p3[0],p1[0]],[p1[1],p2[1],p3[1],p1[1]],linewidth=1,
             color='red')

# save the plot as an image
plt.savefig(outfile)

