#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long long uint64;

int main(int argc, char **argv) {

    /* get N and thread_count from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","thread_count");
        return 1;
    }
    uint64 N = atol(argv[1]);
    int thread_count = atoi(argv[2]);

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* calculate the thread sums in parallel */
    uint64 thread_sums[thread_count];

#pragma omp parallel num_threads(thread_count)
    {
        int thread_num = omp_get_thread_num();
        thread_sums[thread_num] = 0;
#pragma omp for
        for (uint64 i = 1; i <= N;i++) {
            thread_sums[thread_num] += i;
        }
    }

    /* add the results of the thread_sums */
    uint64 sum = 0;
    for (int i=0;i<thread_count;i++) {
        sum += thread_sums[i];
    }    

    /* stop the timer */
    end_time = omp_get_wtime();

#ifdef TIMING
    printf ("(%d,%.4f),",thread_count,end_time-start_time);
#else
    printf ("thread_count = %d, ",thread_count);
    printf ("elapsed time = %g\n",end_time-start_time);
    printf (" N = %lu, ",N);
    printf ("sum = %lu, ",sum);
    printf ("N*(N+1)/2 = %lu\n",(N/2)*(N+1));
#endif

}
