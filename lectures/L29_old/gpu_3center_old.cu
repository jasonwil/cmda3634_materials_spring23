#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>

#define MAX_NUM_POINTS 1000

float dist_sq (float* u, float* v) {
    float diff_x = u[0] - v[0];
    float diff_y = u[1] - v[1];
    return diff_x*diff_x + diff_y*diff_y;
}

float center_cost_sq (int n, float* points, int c1, int c2, int c3) {
    float cost_sq = 0;
    for (int i=0;i<n;i++) {
        float dist_sq_1 = dist_sq (points+2*i,points+2*c1);
        float dist_sq_2 = dist_sq (points+2*i,points+2*c2);
        float dist_sq_3 = dist_sq (points+2*i,points+2*c3);
        float min_dist_sq = dist_sq_1;
        if (dist_sq_2 < min_dist_sq) {
            min_dist_sq = dist_sq_2;
        }
        if (dist_sq_3 < min_dist_sq) {
            min_dist_sq = dist_sq_3;
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
        }
    }
    return cost_sq;
}

int read_file (float* points, char* filename) {

    int n = 0;
    float next[2];
    FILE* file_ptr;
    file_ptr = fopen(filename,"r");
    if (file_ptr == NULL) {
	printf ("error : could not open file %s for reading\n",filename);
	exit(1);
    }
    while (fscanf (file_ptr,"%f %f",next,next+1) == 2) {
        if (n < MAX_NUM_POINTS) {
	    points[2*n] = next[0];
	    points[2*n+1] = next[1];
            n += 1;
        } else {
            printf ("Too many points in file %s\n",filename);
	    fclose (file_ptr);
            exit(1);
        }
    }
    fclose (file_ptr);
    return n;
}

int main (int argc, char** argv) {

    float points[2*MAX_NUM_POINTS];

    /* read filename from command line */
    if (argc < 2) {
	printf ("Command usage : %s %s\n",argv[0],"filename");
	return 1;
    }

    /* read dataset */
    int n = read_file (points,argv[1]);
    printf ("minimal cost = %g\n",sqrt(center_cost_sq(n,points,7,26,97)));
    printf ("n = %d\n",n);

    int block_num = 0;
    int triple_num = 0;
    int thread_num = 0;
    int full_thread_blocks = 0;
    int idle_threads = 0;
    int m = (n+4-1)/4;
    int max_num_blocks = m*(m-1)*(m-2)/6 + 2*m*(m-1)/2 + m;
    for (int i=0;i<n;i+=4) {
	for (int j=i;j<n;j+=4) {
	    for (int k=j;k<n;k+=4) {
		//		printf ("%d %d %d\n",i,j,k);
		block_num += 1;
		thread_num = 0;
		for (int di=0;di<4;di++) {
		    for (int dj=0;dj<4;dj++) {
			for (int dk=0;dk<4;dk++) {
			    if ((i+di < j+dj) && (j+dj < k+dk) && (k+dk<n)) {
				//				printf (" %d %d %d\n",i+di,j+dj,k+dk);
				if (thread_num == 63) {
				    full_thread_blocks += 1;
				}
				thread_num += 1;
				triple_num += 1;
			    } else {
				idle_threads += 1;
			    }
			}
		    }
		}
	    }
	}
    }
    printf ("%d max thread blocks\n",max_num_blocks);
    printf ("%d thread blocks\n",block_num);
    printf ("%d triples checked\n",triple_num);
    printf ("%d full thread blocks\n",full_thread_blocks);
    printf ("%d idle threads\n",idle_threads);
}

