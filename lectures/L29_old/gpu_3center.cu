#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <cuda.h>

#define MAX_NUM_POINTS 2000

int read_file (float* points, char* filename) {

    int n = 0;
    float next[2];
    FILE* file_ptr;
    file_ptr = fopen(filename,"r");
    if (file_ptr == NULL) {
	printf ("error : could not open file %s for reading\n",filename);
	exit(1);
    }
    while (fscanf (file_ptr,"%f %f",next,next+1) == 2) {
        if (n < MAX_NUM_POINTS) {
	    points[2*n] = next[0];
	    points[2*n+1] = next[1];
            n += 1;
        } else {
            printf ("Too many points in file %s\n",filename);
	    fclose (file_ptr);
            exit(1);
        }
    }
    fclose (file_ptr);
    return n;
}

__device__ float dist_sq (float* u, float* v) {
    float diff_x = u[0] - v[0];
    float diff_y = u[1] - v[1];
    return diff_x*diff_x + diff_y*diff_y;
}

__device__ float center_cost_sq (int n, float* points, int c1, int c2, int c3) {
    float cost_sq = 0;
    for (int i=0;i<n;i++) {
        float dist_sq_1 = dist_sq (points+2*i,points+2*c1);
        float dist_sq_2 = dist_sq (points+2*i,points+2*c2);
        float dist_sq_3 = dist_sq (points+2*i,points+2*c3);
        float min_dist_sq = dist_sq_1;
        if (dist_sq_2 < min_dist_sq) {
            min_dist_sq = dist_sq_2;
        }
        if (dist_sq_3 < min_dist_sq) {
            min_dist_sq = dist_sq_3;
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
        }
    }
    return cost_sq;
}


__global__ void center3Kernel(int n, float* d_points, int* d_block_starts) {

    //    __shared__ float s_points[2*MAX_NUM_POINTS];

    int tx = threadIdx.x;
    int ty = threadIdx.y;
    int tz = threadIdx.z;
    int b = blockIdx.x;
    int c1 = d_block_starts[3*b] + tx;
    int c2 = d_block_starts[3*b+1] + ty;
    int c3 = d_block_starts[3*b+2] + tz;

    int num = (2*n + 64 - 1)/64;
    //    for (int i=0;i<64*num;i+=64) {
    //	s_points[i+tz*16+ty*4+tx] = d_points[i+tz*16+ty*4+tx];
    //    }
    float cost_sq = FLT_MAX;
    if ((c1 < c2) && (c2 < c3) && (c3 < n)) {
	cost_sq = center_cost_sq (n,d_points,c1,c2,c3);
    }
    if ((c1 == 210) && (c2 == 979) && (c3 == 1572)) {
	printf ("num = %d\n",num);
	printf ("minimum cost = %g\n",sqrt(cost_sq));
    }
}

int main (int argc, char** argv) {

    float points[2*MAX_NUM_POINTS];

    /* read filename from command line */
    if (argc < 2) {
	printf ("Command usage : %s %s\n",argv[0],"filename");
	return 1;
    }

    /* read dataset */
    int n = read_file (points,argv[1]);

    /* We will use 3d thread blocks of 4^3 = 64 threads */
    dim3 B(4,4,4);

    /* calculate the number of thread blocks needed */
    int m = (n+4-1)/4;
    int G = m*(m-1)*(m-2)/6 + 2*m*(m-1)/2 + m;

    /* print some info */
    printf ("n = %lu\n",n);
    printf ("blockDim B = (%d,%d,%d)\n",B.x,B.y,B.z); 
    printf ("number of thread blocks G = %d\n",G);
    printf ("number of threads = %d\n",G*64);

    /* prepare block_starts array on host */
    int* block_starts = (int*)malloc(3*G*sizeof(int));
    int block_num = 0;
    for (int i=0;i<n;i+=4) {
	for (int j=i;j<n;j+=4) {
	    for (int k=j;k<n;k+=4) {
		block_starts[3*block_num] = i;
		block_starts[3*block_num+1] = j;
		block_starts[3*block_num+2] = k;
		block_num += 1;
	    }
	}
    }

    /* allocate points and block_starts array on device */
    float* d_points;
    int* d_block_starts;
    cudaMalloc (&d_points,2*n*sizeof(float));
    cudaMalloc (&d_block_starts,3*G*sizeof(int));

    /* copy points block_starts from host to device */
    cudaMemcpy (d_points, points, 2*n*sizeof(float), 
		cudaMemcpyHostToDevice);
    cudaMemcpy (d_block_starts, block_starts, 3*G*sizeof(int),
		cudaMemcpyHostToDevice);

    /* launch kernels */
    center3Kernel <<< G, B >>> (n,d_points,d_block_starts);

    /* synchronize */
    cudaDeviceSynchronize();

}

