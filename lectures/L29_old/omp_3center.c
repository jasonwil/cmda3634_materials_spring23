#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <omp.h>

#define MAX_POINTS 2000

typedef struct vec2_s {
    double x, y;
} vec2_type;

double vec2_dist_sq (vec2_type u, vec2_type v) {
    double diff_x = u.x - v.x;
    double diff_y = u.y - v.y;
    return diff_x*diff_x + diff_y*diff_y;
}

typedef struct vec2set_s {
    vec2_type v[MAX_POINTS];
    int size;
} vec2set_type;

void vec2set_read (vec2set_type* set) {
    vec2_type u;
    set->size = 0;
    while (scanf ("%lf %lf",&(u.x),&(u.y)) == 2) {
        if (set->size < MAX_POINTS) {
            set->v[set->size] = u;
            set->size += 1;
        } else {
            printf ("Too many points in stdin\n");
            exit(1);
        }
    }
}

double center_cost_sq (vec2set_type* set, int c1, int c2, int c3) {
    double cost_sq = 0;
    for (int i=0;i<set->size;i++) {
	double dist_sq_1 = vec2_dist_sq (set->v[i],set->v[c1]);
	double dist_sq_2 = vec2_dist_sq (set->v[i],set->v[c2]);
	double dist_sq_3 = vec2_dist_sq (set->v[i],set->v[c3]);
	double min_dist_sq = dist_sq_1;
	if (dist_sq_2 < min_dist_sq) {
	    min_dist_sq = dist_sq_2;
	}
	if (dist_sq_3 < min_dist_sq) {
	    min_dist_sq = dist_sq_3;
	}
	if (min_dist_sq > cost_sq) {
	    cost_sq = min_dist_sq;
	}
    }
    return cost_sq;
}

int main (int argc, char** argv) {

    vec2set_type set;
    vec2set_read (&set);
    
    /* get thread_count from command line */
    if (argc < 2) {
	printf ("Command usage : %s %s\n",argv[0],"thread_count");
	return 1;
    }
    int thread_count = atoi(argv[1]);
    
    /* the overall optimal cost sq, optimal centers, and tuples checked */
    double optimal_cost_sq = DBL_MAX;
    int optimal_centers[3];
    int tuples_checked = 0;

    /* print out the thread count */
#ifndef TIMING
    printf ("thread_count = %d\n",thread_count);
#endif

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();
    
    /* solve the 3-center problem exactly */
#pragma omp parallel num_threads(thread_count)
    {
	double thread_optimal_cost_sq = DBL_MAX;
	int thread_optimal_centers[3];
	int thread_tuples_checked = 0;
	int thread_num = omp_get_thread_num();
	
#ifdef DYNAMIC 
#pragma omp for schedule(dynamic)
#else
#pragma omp for schedule(static)
#endif
	for (int i=0;i<set.size-2;i++) {
	    for (int j=i+1;j<set.size-1;j++) {
		for (int k=j+1;k<set.size;k++) {
		    thread_tuples_checked += 1;
		    double cost_sq = center_cost_sq (&set, i, j, k);
		    if (cost_sq < thread_optimal_cost_sq) {
			thread_optimal_cost_sq = cost_sq;
			thread_optimal_centers[0] = i;
			thread_optimal_centers[1] = j;
			thread_optimal_centers[2] = k;
		    }
		}
	    }
	}
#ifndef TIMING
	printf (" thread %d checked %d 3-tuples\n",
		thread_num,thread_tuples_checked);
#endif	
#pragma omp critical
	{
	    tuples_checked += thread_tuples_checked;
	    if (thread_optimal_cost_sq < optimal_cost_sq) {
		optimal_cost_sq = thread_optimal_cost_sq;
		optimal_centers[0] = thread_optimal_centers[0];
		optimal_centers[1] = thread_optimal_centers[1];
		optimal_centers[2] = thread_optimal_centers[2];
	    }
	}
    }
    
    /* stop the timer */
    end_time = omp_get_wtime();

#ifdef TIMING

    printf ("(%d,%.4f),",thread_count,(end_time-start_time));

#else

    /* print out the number of 3-tuples checked */
    printf (" total number of 3-tuples checked = %d\n",tuples_checked);

    /* print the minimal cost for the 3-center problem */
    printf (" minimal cost = %g\n",sqrt(optimal_cost_sq));
  
    /* print an optimal solution to the 3 center problem */
    printf (" optimal centers : %d %d %d\n",
	    optimal_centers[0],optimal_centers[1],optimal_centers[2]);

    /* print out wall time used */
    printf (" wall time used = %g sec\n",end_time-start_time);

#endif

}
