import sys
import numpy as np
from mpi4py import MPI

# initialize MPI 
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# read the data file containing 2d points
points = np.loadtxt(sys.argv[1],dtype=np.float32)

# compute the distance squared between two points
def distance_sq(p1,p2):
    return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])

# compute the cost squared given k centers
def centers_cost_sq(points,centers):
    cost_sq = 0
    n = len(points)
    k = len(centers)
    for i in range(n):
        min_dist_sq = float("inf")
        for j in range(k):
            dist_sq = distance_sq(points[i],points[centers[j]])
            if (dist_sq < min_dist_sq):
                min_dist_sq = dist_sq
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
    return cost_sq

# approximate a solution to the k-center problem
def approx_kcenter(points,guesses,centers):
    min_cost_sq = float("inf")
    n = len(points)
    m = len(guesses)
    k = len(centers)
    for i in range(m):
        cost_sq = centers_cost_sq(points,guesses[i])
        if (cost_sq < min_cost_sq):
            min_cost_sq = cost_sq
            np.copyto(centers,guesses[i])
    return np.sqrt(min_cost_sq)

# Approximate the k-center problem using pure Python
k = int(sys.argv[2])
m = int(sys.argv[3])
seed = int(sys.argv[4])+rank
centers = np.zeros(k,dtype='int32')
n = len(points)
np.random.seed(seed)
guesses = np.random.rand(m,n).argpartition(k,axis=1)[:,:k].astype(np.int32)
min_cost = approx_kcenter(points,guesses,centers)
print ('rank',rank,'approximate kcenter solution is',centers)
print ('rank',rank,'approximate minimal cost is',np.round(min_cost,5))

all_centers = comm.gather(centers,root=0)
if (rank == 0):
    for i in range(1,size):
        cost_sq = np.sqrt(centers_cost_sq(points,all_centers[i]))
        if (cost_sq < min_cost):
            min_cost = cost_sq
            np.copyto(centers,all_centers[i])
    print ('\nfinal approximate kcenter solution is',centers)
    print ('final approximate minimal cost is',np.round(min_cost,5))
