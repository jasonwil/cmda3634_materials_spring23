#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

typedef unsigned long int uint64;
typedef unsigned __int128 uint128;

/* compute the total stopping time for a given number n */
uint64 total_stopping_time (uint64 n) {
    uint128 a_i = (uint128)n;
    uint64 total = 0;
    while (a_i != 1) {
	if (a_i % 2 == 0) {
	    a_i = a_i/2;
	} else {
	    a_i = (3*a_i+1);
	}
	total += 1;
    }
    return total;
}

int main(int argc, char **argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    /* get N from command line */
    if (argc != 2) {
	printf ("Command usage : %s %s\n",argv[0],"N");
	return 1;
    }
    uint64 N = (uint64)atol(argv[1]);

    uint64 max_total[2];
    max_total[0] = 1;
    max_total[1] = 0;
    for (uint64 n = 1+rank;n<=N;n+=size) {
	uint64 total = total_stopping_time(n);
	if (total > max_total[1]) {
	    max_total[0] = n;
	    max_total[1] = total;
	}
    }

    uint64 max_totals[2*size];
    MPI_Gather (max_total,2,MPI_UNSIGNED_LONG_LONG,max_totals,2,
		MPI_UNSIGNED_LONG_LONG,0,MPI_COMM_WORLD);
    if (rank == 0) {
	for (int i=1;i<size;i++) {
	    if (max_totals[2*i+1] > max_total[1]) {
		max_total[0] = max_totals[2*i];
		max_total[1] = max_totals[2*i+1];
	    }
	}
    }

    /* stop timer */
    end_time = MPI_Wtime();

    if (rank == 0) {

	/* print wall time */
	printf ("wall time used = %.4f sec\n",(end_time-start_time));

	/* output the results */
	printf ("The starting value less than or equal to %lu\n",N);
	printf ("  having the largest total stopping time is %lu\n",max_total[0]);
	printf ("  which has %lu steps\n",max_total[1]);
    }
}



