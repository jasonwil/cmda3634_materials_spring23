#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <mpi.h>

#define MAX_POINTS 2000

typedef struct vec2_s {
    double x, y;
} vec2_type;

double vec2_dist_sq (vec2_type u, vec2_type v) {
    double diff_x = u.x - v.x;
    double diff_y = u.y - v.y;
    return diff_x*diff_x + diff_y*diff_y;
}

typedef struct vec2set_s {
    vec2_type v[MAX_POINTS];
    int size;
} vec2set_type;

void vec2set_read_file (vec2set_type* set, char* filename) {
    vec2_type u;
    set->size = 0;
    FILE* file_ptr;
    file_ptr = fopen(filename,"r");
    if (file_ptr == NULL) {
	printf ("error : could not open file %s for reading\n",filename);
	exit(1);
    }
    while (fscanf (file_ptr,"%lf %lf",&(u.x),&(u.y)) == 2) {
        if (set->size < MAX_POINTS) {
            set->v[set->size] = u;
            set->size += 1;
        } else {
            printf ("Too many points in file %s\n",filename);
	    fclose (file_ptr);
            exit(1);
        }
    }
    fclose (file_ptr);
}

/* calculate the cost squared of a given set of center locations */
double center_cost_sq (vec2set_type* set, int* centers, int k) {
    double cost_sq = 0;
    for (int i=0;i<set->size;i++) {
	double min_dist_sq = DBL_MAX;
	for (int j=0;j<k;j++) {
	    double dist_sq = vec2_dist_sq(set->v[i],set->v[centers[j]]);
	    if (dist_sq < min_dist_sq) {
		min_dist_sq = dist_sq;
	    }
	}
	if (min_dist_sq > cost_sq) {
	    cost_sq = min_dist_sq;
	}
    }
    return cost_sq;
}

typedef struct centers_s {
    double cost_sq;
    int centers[8];
} centers_type;

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* read filename from command line */
    if (argc < 2) {
	printf ("Command usage : %s %s\n",argv[0],"filename");
	return 1;
    }

    /* read dataset */
    vec2set_type set;
    vec2set_read_file (&set,argv[1]);

    /* start the timer */
    double start_time;
    start_time = MPI_Wtime();

    /* solve the 8-center problem exactly */
    int c[8];
    centers_type optimal = { DBL_MAX, { 0, 0, 0, 0, 0, 0, 0, 0 } };
    long int rank_tuples_checked = 0;
    long int tuple_num = 0;
    for (c[0]=0;c[0]<set.size-7;c[0]++) {
	for (c[1]=c[0]+1;c[1]<set.size-6;c[1]++) {
	    for (c[2]=c[1]+1;c[2]<set.size-5;c[2]++) {
		for (c[3]=c[2]+1;c[3]<set.size-4;c[3]++) {
		    for (c[4]=c[3]+1;c[4]<set.size-3;c[4]++) {
			for (c[5]=c[4]+1;c[5]<set.size-2;c[5]++) {
			    for (c[6]=c[5]+1;c[6]<set.size-1;c[6]++) {
				for (c[7]=c[6]+1;c[7]<set.size;c[7]++) {
				    if (tuple_num % size == rank) {
					rank_tuples_checked += 1;
					double cost_sq = center_cost_sq(&set,c,8);
					if (cost_sq < optimal.cost_sq) {
					    optimal.cost_sq = cost_sq;
					    for (int i=0;i<8;i++) {
						optimal.centers[i] = c[i];
					    }
					}
				    }
				    tuple_num += 1;
				}
			    }
			}
		    }
		}
	    }
	}
    }
    //    printf ("rank %d checked %ld tuples\n",rank,rank_tuples_checked);

    /* create the MPI centers datatype */
    MPI_Datatype MPI_centers_type;
    int lengths[2] = { 1, 8 };
    MPI_Aint addresses[3];
    MPI_Aint displacements[2];
    MPI_Datatype types[2] = { MPI_DOUBLE, MPI_INT };
    MPI_Get_address (&optimal,&addresses[0]);
    MPI_Get_address (&optimal.cost_sq, &addresses[1]);
    MPI_Get_address (&optimal.centers, &addresses[2]);
    displacements[0] = MPI_Aint_diff(addresses[1], addresses[0]);
    displacements[1] = MPI_Aint_diff(addresses[2], addresses[0]);
    MPI_Type_create_struct(2, lengths, displacements, types, &MPI_centers_type);
    MPI_Type_commit(&MPI_centers_type);

    /* gather the optimal centers onto rank 0 */
    centers_type optimal_centers[size];
    MPI_Gather (&optimal,1,MPI_centers_type,optimal_centers,1,MPI_centers_type,
		0,MPI_COMM_WORLD);

    /* rank 0 finds the optimal centers */
    if (rank == 0) {
        for (int i=1;i<size;i++) {
            if (optimal_centers[i].cost_sq < optimal.cost_sq) {
                optimal = optimal_centers[i];
            }
        }
    }

    /* stop the timer */
    double end_time;
    end_time = MPI_Wtime();

    /* add up the number of tuples checked */
    long int tuples_checked;
    MPI_Reduce (&rank_tuples_checked,&tuples_checked,1,MPI_LONG_LONG_INT,
		MPI_SUM,0,MPI_COMM_WORLD);

    /* rank 0 prints the results */
    if (rank == 0) {

	/* print out the number of ranks */
	printf ("rank count = %d\n",size);

	/* print out the elapsed time */
        printf (" elapsed time = %g seconds\n",end_time-start_time);

	/* print out the number of 8-tuples checked */
	printf (" number of 8-tuples checked = %ld\n",tuples_checked);

	/* print the optimal cost for the 8-center problem */
	printf (" optimal cost = %g\n",sqrt(optimal.cost_sq));
  
	/* print an optimal solution to the 8-center problem */
	printf (" optimal centers : ");
	for (int i=0;i<8;i++) {
	    printf ("%d ",optimal.centers[i]);
	}
	printf ("\n");
    }

    MPI_Finalize();
}
