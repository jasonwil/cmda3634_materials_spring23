#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

#define MAX_POINTS 2000

typedef struct vec2_s {
    double x, y;
} vec2_type;

double vec2_dist_sq (vec2_type u, vec2_type v) {
    double diff_x = u.x - v.x;
    double diff_y = u.y - v.y;
    return diff_x*diff_x + diff_y*diff_y;
}

typedef struct vec2set_s {
    vec2_type v[MAX_POINTS];
    int size;
} vec2set_type;

void vec2set_read_file (vec2set_type* set, char* filename) {
    vec2_type u;
    set->size = 0;
    FILE* file_ptr;
    file_ptr = fopen(filename,"r");
    if (file_ptr == NULL) {
	printf ("error : could not open file %s for reading\n",filename);
	exit(1);
    }
    while (fscanf (file_ptr,"%lf %lf",&(u.x),&(u.y)) == 2) {
        if (set->size < MAX_POINTS) {
            set->v[set->size] = u;
            set->size += 1;
        } else {
            printf ("Too many points in file %s\n",filename);
	    fclose (file_ptr);
            exit(1);
        }
    }
    fclose (file_ptr);
}

/* calculate the cost squared of a given set of center locations */
double center_cost_sq (vec2set_type* set, int* centers, int k) {
    double cost_sq = 0;
    for (int i=0;i<set->size;i++) {
	double min_dist_sq = DBL_MAX;
	for (int j=0;j<k;j++) {
	    double dist_sq = vec2_dist_sq(set->v[i],set->v[centers[j]]);
	    if (dist_sq < min_dist_sq) {
		min_dist_sq = dist_sq;
	    }
	}
	if (min_dist_sq > cost_sq) {
	    cost_sq = min_dist_sq;
	}
    }
    return cost_sq;
}

int main (int argc, char** argv) {

    /* read filename from command line */
    if (argc < 2) {
	printf ("Command usage : %s %s\n",argv[0],"filename");
	return 1;
    }

    /* read dataset */
    vec2set_type set;
    vec2set_read_file (&set,argv[1]);

    /* solve the 8-center problem exactly */
    int c[8];
    int optimal_centers[8];
    double optimal_cost_sq = DBL_MAX;
    long int tuples_checked = 0;
    for (c[0]=0;c[0]<set.size-7;c[0]++) {
	for (c[1]=c[0]+1;c[1]<set.size-6;c[1]++) {
	    for (c[2]=c[1]+1;c[2]<set.size-5;c[2]++) {
		for (c[3]=c[2]+1;c[3]<set.size-4;c[3]++) {
		    for (c[4]=c[3]+1;c[4]<set.size-3;c[4]++) {
			for (c[5]=c[4]+1;c[5]<set.size-2;c[5]++) {
			    for (c[6]=c[5]+1;c[6]<set.size-1;c[6]++) {
				for (c[7]=c[6]+1;c[7]<set.size;c[7]++) {
				    tuples_checked += 1;
				    double cost_sq = center_cost_sq(&set,c,8);
				    if (cost_sq < optimal_cost_sq) {
					optimal_cost_sq = cost_sq;
				    	for (int i=0;i<8;i++) {
				    	    optimal_centers[i] = c[i];
				    	}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }

    /* print out the number of 8-tuples checked */
    printf ("number of 8-tuples checked = %ld\n",tuples_checked);

    /* print the optimal cost for the 8-center problem */
    printf ("optimal cost = %g\n",sqrt(optimal_cost_sq));
  
    /* print an optimal solution to the 8-center problem */
    printf ("optimal centers : ");
    for (int i=0;i<8;i++) {
	printf ("%d ",optimal_centers[i]);
    }
    printf ("\n");

}
