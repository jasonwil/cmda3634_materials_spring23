#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>

typedef unsigned long long int uint64;

__global__ void stdevKernel(uint64 N) {

    /* compute the mean */
    uint64 sum = 0;
    for (uint64 i=1;i<=N;i++) {
        sum += i;
    }
    double mean = (1.0)*sum/N;

    /* compute the sum of differences squared */
    double sum_diff_sq = 0;
    for (uint64 i=1;i<=N;i++) {
        sum_diff_sq += (i-mean)*(i-mean);
    }

    /* print the sample standard deviation */
    printf (" Sample standard deviation is %g\n",sqrt(sum_diff_sq/(N-1)));
}

int main (int argc, char** argv) {

    /* get N and num_threads from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }

    uint64 N = atol(argv[1]);
    int num_threads = atoi(argv[2]);

    printf ("num_threads = %d\n",num_threads); 
    stdevKernel <<< 1, num_threads >>> (N);
    cudaDeviceSynchronize();

}
