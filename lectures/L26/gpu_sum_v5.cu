#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

typedef unsigned long long int uint64;

__global__ void sumKernel(uint64 N) {

    __shared__ uint64 sum;

    int thread_num = threadIdx.x;
    int num_threads = blockDim.x;

    /* initialize sum to 0 */
    if (thread_num == 0) {
        sum = 0;
    }
    __syncthreads();

    /* calculate the sum */
    for (uint64 i = 1+thread_num; i <= N;i+=num_threads) {
        atomicAdd(&sum,i);
    }
    __syncthreads();

    /* thread 0 prints the sum */
    if (thread_num == 0) {
        printf (" sum = %lu\n",sum);
    }
}

int main(int argc, char **argv) {

    /* get N and num_threads from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }

    uint64 N = atol(argv[1]);
    int num_threads = atoi(argv[2]);

    printf ("num_threads = %d\n",num_threads); 
    printf ("N*(N+1)/2 = %lu\n",(N/2)*(N+1));

    sumKernel <<< 1, num_threads >>> (N);
    cudaDeviceSynchronize();

}

