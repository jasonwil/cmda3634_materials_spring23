#include <stdio.h>
#include <stdlib.h>

typedef unsigned long long int uint64;

int main(int argc, char **argv) {

    /* get N and num_threads from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }

    uint64 N = atol(argv[1]);
    int num_threads = atoi(argv[2]);

    uint64 sum = 0;
    for (uint64 i = 1; i <= N;i++) {
        sum += i;
    }

    printf ("N*(N+1)/2 = %lu\n",(N/2)*(N+1));
    printf (" sum = %lu\n",sum);
}

