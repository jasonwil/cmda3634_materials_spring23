#ifndef STACK_H
#define STACK_H

#define MAX_STACK_SIZE 100

typedef struct stack_s {
    int data[MAX_STACK_SIZE];
    int size;
} stack_type;

/* initializes a stack to be empty */
stack_type stack_init (stack_type stack);

/* returns the top element of the stack */
/* exits with an error if the stack is empty */
int stack_top (stack_type stack);

/* pushes the given element onto the top of the stack */
/* exits with an error if the stack is full */
stack_type stack_push (stack_type stack, int element);

/* pops the top element off of the stack */
/* exits with an error if the stack is empty */
stack_type stack_pop (stack_type stack);


#endif
