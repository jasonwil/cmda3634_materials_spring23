#include <stdio.h>
#include "stack.h"

int main () {
    stack_type stack;
    stack = stack_init (stack);
    int next;
    for (int i=15;i<MAX_STACK_SIZE;i++) {
	stack = stack_push (stack,i);
    }
    for (int i=15;i<MAX_STACK_SIZE;i++) {
	next = stack_top (stack);
	stack = stack_pop (stack);
    }
    printf ("The last number is %d\n",next);
}
