#include <stdio.h>
#include "vec2.h"
#include "data.h"

int main () {
    vec2_type mean = { 0, 0 };
    for (int i = 0;i<NUM_POINTS;i++) {
        mean = vec2_add (mean, data[i]); 
    }
    mean = vec2_scalar_mult (mean, 1.0/NUM_POINTS);
    printf ("mean of data = ");
    vec2_print (mean);
}
