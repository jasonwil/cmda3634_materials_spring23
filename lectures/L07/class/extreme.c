#include <stdio.h>
#include <math.h>
#include "vec2.h"
#include "data.h"

typedef struct pair_s {
    double dist_sq;
    int i,j;
} pair_type;

pair_type find_extreme_pair () {
    pair_type extreme_pair = { 0, -1, -1 };
    for (int i = 0;i<NUM_POINTS-1;i++) {
        for (int j = i+1;j<NUM_POINTS;j++) {
            double dist_sq = vec2_dist_sq (data[i],data[j]);
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.i = i;
                extreme_pair.j = j;
            }
        }
    }
    return extreme_pair;
}

int main () {

    /* find the extreme pair */
    pair_type extreme_pair = find_extreme_pair ();

    /* output the results */
    printf ("Extreme Distance : %g\n",sqrt(extreme_pair.dist_sq));
    printf ("Extreme Indices : %d %d\n",extreme_pair.i,extreme_pair.j);

}
