#include <stdio.h>
#include "vec2.h"

void vec2_print (vec2_type v) {
    printf ("%g %g\n",v.x, v.y);
}

vec2_type vec2_add (vec2_type v, vec2_type w) {
    vec2_type sum = { v.x + w.x, v.y + w.y };
    return sum;
}

vec2_type vec2_scalar_mult (vec2_type v, double c) {
    vec2_type prod = { c*v.x ,c*v.y };
    return prod;
}

double vec2_dist_sq (vec2_type v, vec2_type w) {
    return (v.x-w.x)*(v.x-w.x)+(v.y-w.y)*(v.y-w.y);
}
