#include <stdio.h>
#include <math.h>
#include "vec2.h"
#include "data.h"

typedef struct pair_s {
    double dist_sq;
    int i,j;
} pair_type;

/* fix this function! */
pair_type find_extreme_pair () {
    pair_type extreme_pair = { 0, -1, -1 };
    return extreme_pair;
}

int main () {

    /* find the extreme pair */
    pair_type extreme_pair = find_extreme_pair ();

    /* output the results */
    printf ("Extreme Distance : %g\n",sqrt(extreme_pair.dist_sq));
    printf ("Extreme Indices : %d %d\n",extreme_pair.i,extreme_pair.j);

}
