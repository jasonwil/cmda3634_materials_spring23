#include <stdio.h>
#include <stdlib.h>

int main (int argc, char** argv) {

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    /* atoi converts a string to an integer value */
    int N = atoi(argv[1]);

    /* compute the start less than or equal to N with largest total stop */
    int max_start = -1;
    int max_total = -1;

    /* print results */
    printf ("The starting value less than or equal to %d\n",N);
    printf ("  having the largest total stopping time is %d\n",max_start);
    printf ("  which has %d steps\n",max_total);

    return 0;
}
