import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 2):
    print ("Command Usage : python3",sys.argv[0],"outfile")
    exit(1)
outfile = sys.argv[1]

# read the data file
data = np.loadtxt(sys.stdin)

# plot the data
plt.gca().set_aspect('equal')
plt.scatter(data[:,0],data[:,1],s=10,color='black')

# plot the special points (if additional command line argments present)
if (len(sys.argv) > 2):
    x = float(sys.argv[2])
    y = float(sys.argv[3])
    plt.scatter ([x],[y],s=50,color='orange')

# save the plot as an image
plt.savefig(outfile)

