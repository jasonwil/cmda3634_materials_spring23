#include <stdio.h>
#include <stdlib.h>

/* write a function to */
/* compute the total stopping time of a given start value */
int total_stopping_time (int start) {
    int total = 0;
    long int a_i = start;
    while (a_i != 1) {
        total += 1;
        if (a_i % 2 == 0) {
            a_i /= 2;
        } else {
            a_i = 3*a_i + 1;
        }
    }
    return total;
}

typedef struct start_total_s {
    int start;
    int total;
} start_total_type;

start_total_type max_total_stopping_time (int N) {

    start_total_type max = { 1, 0 };
    
    for (int start=1; start <= N; start++) {
        int total = total_stopping_time (start);
        if (total > max.total) {
            max.total = total;
            max.start = start;
        }
    }
    return max;
}

int main (int argc, char** argv) {

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    /* atoi converts a string to an integer value */
    int N = atoi(argv[1]);

    start_total_type max = max_total_stopping_time(N);

    /* print results */
    printf ("The starting value less than or equal to %d\n",N);
    printf ("  having the largest total stopping time is %d\n",max.start);
    printf ("  which has %d steps\n",max.total);

    return 0;
}
