#include <stdio.h>
#include "vec2.h"

int main () {
    vec2_type v = { 3.9, 4.7 };
    vec2_type w = { 2.1, -1.7 };
    vec2_type x = vec2_add(v,w);
    vec2_print (x);
}
