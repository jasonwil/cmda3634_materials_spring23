#include <stdio.h>
#include <stdlib.h>

/* write a function to */
/* compute the total stopping time of a given start value */
int total_stopping_time (int start) {
    int total = 0;
    long int a_i = start;
    while (a_i != 1) {
        total += 1;
        if (a_i % 2 == 0) {
            a_i /= 2;
        } else {
            a_i = 3*a_i + 1;
        }
    }
    return total;
}

int main (int argc, char** argv) {

    /* get start from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"start");
        return 1;
    }
    /* atoi converts a string to an integer value */
    int start = atoi(argv[1]);

    /* need to fix this! */
    int total = total_stopping_time(start);

    /* print results */
    printf ("The total stopping time of %d is %d\n",start,total);

    return 0;
}
