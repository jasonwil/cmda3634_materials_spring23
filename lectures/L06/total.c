#include <stdio.h>
#include <stdlib.h>

/* write a function to */
/* compute the total stopping time of a given start value */

int main (int argc, char** argv) {

    /* get start from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"start");
        return 1;
    }
    /* atoi converts a string to an integer value */
    int start = atoi(argv[1]);

    /* need to fix this! */
    int total = -1;

    /* print results */
    printf ("The total stopping time of %d is %d\n",start,total);

    return 0;
}
