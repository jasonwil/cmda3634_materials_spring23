#include <stdio.h>
#include <math.h>
#include <float.h>

float distance_sq(float* p1, float* p2) {
    return ((p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1]));
}

float min3 (float a, float b, float c) {
    float min = a;
    if (b < min) {
        min = b;
    }
    if (c < min) {
        min = c;
    }
    return min;
}

float centers_cost(int num, float* pts, int c1, int c2, int c3) {
    /* add code here */
}

void solve_3center (int num, float* pts, int* centers) {
    /* add code here */
}
