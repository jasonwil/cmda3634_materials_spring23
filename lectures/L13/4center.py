import sys
import numpy as np
import matplotlib.pyplot as plt

# read the cities data file
cities = np.loadtxt(sys.stdin,dtype=np.float32)

# compute the distance squared between two points
def distance_sq(p1,p2):
    return (p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1])

# return the minimum of the 4 input values
def min4(a,b,c,d):
    min = a
    if (b < min):
        min = b
    if (c < min):
        min = c
    if (d < min):
        min = d
    return min

# compute the cost given four centers
def centers_cost(pts,c1,c2,c3,c4):
    cost_sq = 0
    num = len(pts)
    for i in range(num):
        dist_sq_1 = distance_sq(pts[i],pts[c1])
        dist_sq_2 = distance_sq(pts[i],pts[c2])
        dist_sq_3 = distance_sq(pts[i],pts[c3])
        dist_sq_4 = distance_sq(pts[i],pts[c4])
        min_dist_sq = min4(dist_sq_1,dist_sq_2,dist_sq_3,dist_sq_4)
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
    return np.sqrt(cost_sq)

# solve the 4 center problem
def solve_4center(pts,centers):
    num_checked = 0
    min_cost = float("inf")
    num = len(pts)
    for i in range(0,num-3):
        for j in range(i+1,num-2):
            for k in range(j+1,num-1):
                for l in range(k+1,num):
                    num_checked += 1
                    cost = centers_cost(pts,i,j,k,l)
                    if (cost<min_cost):
                        min_cost = cost
                        centers[0] = i
                        centers[1] = j
                        centers[2] = k
                        centers[3] = l
    print ('number of quadruples checked =',num_checked)

# Solve the 4 center problem using our Python function
centers = np.zeros(4,dtype='int32')
solve_4center(cities,centers)
c1 = centers[0]
c2 = centers[1]
c3 = centers[2]
c4 = centers[3]
print ('4center solution is c1 =',c1,', c2 =',c2,', c3 =',c3,', c4 =',c4)

# compute the cost, clusters, and extreme given three centers
def centers_cost_plus(pts,c1,c2,c3,c4,clusters):
    cost_sq = 0
    num = len(pts)
    for i in range(num):
        dist_sq_1 = distance_sq(pts[i],pts[c1])
        dist_sq_2 = distance_sq(pts[i],pts[c2])
        dist_sq_3 = distance_sq(pts[i],pts[c3])
        dist_sq_4 = distance_sq(pts[i],pts[c4])
        clusters[i] = c1
        min_dist_sq = dist_sq_1
        if (dist_sq_2 < min_dist_sq):
            clusters[i] = c2
            min_dist_sq = dist_sq_2
        if (dist_sq_3 < min_dist_sq):
            clusters[i] = c3
            min_dist_sq = dist_sq_3
        if (dist_sq_4 < min_dist_sq):
            clusters[i] = c4
            min_dist_sq = dist_sq_4
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
            extreme = i
    return (np.sqrt(cost_sq),extreme)

# print the cost of the optimal solution
clusters = np.zeros(len(cities),dtype='int32')
cost, extreme = centers_cost_plus(cities,c1,c2,c3,c4,clusters)
print ('cost for c1 =',c1,', c2 =',c2,', c3 =',c3,', c4 =',c4,'is',cost)

# plot the cities showing centers, clusters, and the extreme point
plt.gca().set_aspect('equal')
plt.scatter(cities[clusters==c1,0],cities[clusters==c1,1],s=10,color='orange')
plt.scatter(cities[clusters==c2,0],cities[clusters==c2,1],s=10,color='maroon')
plt.scatter(cities[clusters==c3,0],cities[clusters==c3,1],s=10,color='teal')
plt.scatter(cities[clusters==c4,0],cities[clusters==c4,1],s=10,color='gray')
plt.scatter(cities[c1,0],cities[c1,1],s=100,color='orange')
plt.scatter(cities[c2,0],cities[c2,1],s=100,color='maroon')
plt.scatter(cities[c3,0],cities[c3,1],s=100,color='teal')
plt.scatter(cities[c4,0],cities[c4,1],s=100,color='gray')
plt.scatter(cities[extreme,0],cities[extreme,1],
        s=50,facecolors='none', edgecolors='black')
plt.savefig("4center.png")

