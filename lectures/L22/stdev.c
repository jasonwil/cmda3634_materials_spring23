#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef long long int int64;

int main (int argc, char** argv) {

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int64 N = atol(argv[1]);

    /* compute the mean */
    double sum = 0;
    for (int64 i=1;i<=N;i++) {
	sum += i;
    }
    double mean = sum/N;

    /* compute the sample variance */
    double sum_diff_sq = 0;
    for (int64 i=1;i<=N;i++) {
	sum_diff_sq += (i-mean)*(i-mean);
    }
    double sample_variance = sum_diff_sq/(N-1);

    /* print the sample standard deviation */
    double sample_stdev = sqrt(sample_variance);
    printf ("The sample standard deviation is %g\n",sample_stdev);
}
