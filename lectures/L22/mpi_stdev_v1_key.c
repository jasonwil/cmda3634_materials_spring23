#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

typedef long long int int64;

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int64 N = atol(argv[1]);

    /* start the timer */
    double start_time;
    start_time = MPI_Wtime();

    /* compute the mean */
    double sum = 0;
    for (int64 i=1+rank;i<=N;i+=size) {
	sum += i;
    }
    double in_msg;
    double out_msg;
    MPI_Status status;
    int source, dest;

    /* rank 0 sums up partial sums from each rank */
    if (rank == 0) {
        for (int i=1;i<size;i++) {
            source = i;
            MPI_Recv (&in_msg, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD,&status);
            sum += in_msg;
        }
    } else {
        out_msg = sum;
        int dest = 0;
        MPI_Send (&out_msg, 1, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
    }

    /* rank 0 broadcasts the total sum to all other ranks */
    if (rank == 0) {
	out_msg = sum;
        for (int i=1;i<size;i++) {
            dest = i;
	    MPI_Send (&out_msg, 1, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
        }
    } else {
        int dest = 0;
	MPI_Recv (&in_msg, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD,&status);
	sum = in_msg;
    } 
    double mean = sum/N;

    /* compute the sample variance */
    double sum_diff_sq = 0;
    for (int64 i=1+rank;i<=N;i+=size) {
	sum_diff_sq += (i-mean)*(i-mean);
    }

    /* rank 0 sums up partial sums from each rank */
    if (rank == 0) {
        for (int i=1;i<size;i++) {
            source = i;
            MPI_Recv (&in_msg, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD,&status);
            sum_diff_sq += in_msg;
        }
    } else {
        out_msg = sum_diff_sq;
        int dest = 0;
        MPI_Send (&out_msg, 1, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
    }
    double sample_variance = sum_diff_sq/(N-1);

    /* stop the timer */
    double end_time;
    end_time = MPI_Wtime();

    /* print the sample standard deviation */
    if (rank == 0) {
	double sample_stdev = sqrt(sample_variance);
	printf ("The sample standard deviation is %g\n",sample_stdev);
        printf ("elapsed time = %g seconds\n",end_time-start_time);
    }

    MPI_Finalize();
}
