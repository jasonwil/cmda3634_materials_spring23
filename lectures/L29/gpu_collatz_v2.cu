#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>

typedef unsigned int uint32;
typedef unsigned long long int uint64;

/* compute the total stopping time for a given number n */
__device__ uint32 total_stopping_time (uint64 a_i) {
    uint32 total = 0;
    while (a_i != 1) {
        if (a_i % 2 == 0) {
            a_i = a_i/2;
        } else {
            a_i = (3*a_i+1);
        }
        total += 1;
    }
    return total;
}

__global__ void collatzKernel(uint64 N, uint32* d_totals) {

    int thread_num = blockIdx.x*blockDim.x + threadIdx.x;
    if (thread_num < N) {
        d_totals[thread_num] = total_stopping_time(thread_num+1);
    }
}

int main (int argc, char** argv) {

    /* B is the number of threads per block */
    /* we typically choose B to be a multiple of 32 */
    /* the maximum value of B is 1024 */
    /* get N and B from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","B");
        return 1;
    }
    uint64 N = atol(argv[1]);
    int B = atoi(argv[2]);

    /* G is the number of thread blocks */
    /* the maximum number of thread blocks G is 2^31 - 1 = 2147483647 */
    /* We choose G to be the minimum number of thread blocks to have at least N threads */
    int G = (N+B-1)/B;
    printf ("N = %lu\n",N);
    printf ("threads per block B = %d\n",B); 
    printf ("number of thread blocks G = %d\n",G);
    printf ("number of threads G*B = %d\n",G*B);

    /* the totals array in host memory */
    uint32* totals = (uint32*)malloc(N*sizeof(uint32));

    /* the totals array in device memory */
    uint32* d_totals;
    cudaMalloc (&d_totals,N*sizeof(uint32));

    /* launch kernels */
    collatzKernel <<< G, B >>> (N,d_totals);

    /* copy total stopping time data from device to host */
    cudaMemcpy (totals, d_totals, N*sizeof(uint32),cudaMemcpyDeviceToHost);

    /* host uses totals array to find the largest total stopping time */
    uint64 max_start = 1;
    uint32 max_total = 0;
    for (uint64 n = 1;n<=N;n++) {
        uint32 total = totals[n-1];
        if (total > max_total) {
            max_start = n;
            max_total = total;
        }
    }

    /* output the results */
    printf ("The starting value less than or equal to %lu\n",N);
    printf ("  having the largest total stopping time is %lu\n",max_start);
    printf ("  which has %u steps\n",max_total);

    /* free the memory on the host */
    free (totals);

    /* free the memory on the device */
    cudaFree (d_totals);
}
