#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mat.h"

typedef struct pair_s {
    double dist_sq;
    int i,j;
} pair_type;

pair_type find_extreme_pair (mat_type* A) {
    pair_type extreme_pair = { 0, -1, -1 };
    vec_type row1, row2;
    for (int i = 0;i<A->rows-1;i++) {
        for (int j = i+1;j<A->rows;j++) {
	    mat_get_row(A,&row1,i);
	    mat_get_row(A,&row2,j);
            double dist_sq = vec_dist_sq(&row1,&row2);
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.i = i;
                extreme_pair.j = j;
            }
        }
    }
    return extreme_pair;
}

int main (int argc, char** argv) {

    /* read the shape of the matrix from STDIN */
    int rows, cols;
    if (scanf("%d %d",&rows,&cols) != 2) {
	printf ("error reading the shape of the matrix\n");
	return 1;
    }

    /* dynamically allocate memory for the (rows x cols) matrix */
    double* A_data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix */
    mat_type A;
    mat_init (&A,A_data,rows,cols);

    /* read the matrix from STDIN */
    int num_read = mat_read_stdin (&A);

    /* find the extreme pair */
    pair_type extreme_pair = find_extreme_pair (&A);

    /* output the results */
    printf ("Extreme Distance : %g\n",sqrt(extreme_pair.dist_sq));
    printf ("Extreme Indices : %d %d\n",extreme_pair.i,extreme_pair.j);

    /* free the dynamically allocated matrix data buffer */
    free (A_data);

    return 0;
}
