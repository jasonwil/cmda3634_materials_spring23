#!/bin/bash
#SBATCH -t 10
#SBATCH -p normal_q
#SBATCH -A cmda3634_rjh
#SBATCH --cpus-per-task=32
#SBATCH -o extreme_omp.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need
module load matplotlib

# Compile the C file 
gcc -o extreme_omp extreme_omp.c mat.c vec.c -lm -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=true

# run the 3center Python script
time cat $1 | ./extreme_omp
