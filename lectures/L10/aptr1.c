#include <stdio.h>

int main () {
    int a[3] =  { 4, 9, 16 };
    printf ("a = { %d, %d, %d }\n",a[0],a[1],a[2]);
    /* we can take the address of any array element */
    int* p = &(a[1]);
    printf ("p = %(a[1]) = %p\n",p);
    /* what will the following lines of code print? */
    printf ("*p = %d\n",*p);
    printf ("p[0] = %d\n",p[0]);
    printf ("p[1] = %d\n",p[1]);
    printf ("p[-1] = %d\n",p[-1]);
}
