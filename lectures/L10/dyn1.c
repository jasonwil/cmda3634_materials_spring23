#include <stdio.h>
#include <stdlib.h>

void vec_print (int* v, int dim) {
    printf ("{ ");
    for (int i=0;i<dim;i++) {
	printf ("%d ",v[i]);
    }
    printf ("}\n");
}

int main () {
    int a[5] = { 1, 4, 9, 16, 25 };
    int dim = 5;
    printf ("as a pointer a = %p\n",a);
    printf ("as an array a = ");
    vec_print (a,dim);

    /* how many bytes of memory do we need to allocate */
    /* for an array of dim integers */
    printf ("sizeof(a) = %d, dim*sizeof(int) = %d\n",
	    sizeof(a),dim*sizeof(int));

}
