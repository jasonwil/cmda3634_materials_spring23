#include <stdio.h>
#include <stdlib.h>

void vec_print (int* v, int dim) {
    printf ("{ ");
    for (int i=0;i<dim;i++) {
	printf ("%d ",v[i]);
    }
    printf ("}\n");
}

int main () {
    int a[5] = { 1, 4, 9, 16, 25 };
    int dim = 5;
    printf ("as a pointer a = %p\n",a);
    printf ("as an array a = ");
    vec_print (a,dim);

    /* how many bytes of memory do we need to allocate */
    /* for an array of dim integers */
    printf ("sizeof(a) = %d, dim*sizeof(int) = %d\n",
	    sizeof(a),dim*sizeof(int));

    /* calloc stands for clear allocate */
    /* in addition to allocating the array, */ 
    /* it initializes the elements to 0 */
    int* v = (int*)calloc(dim,sizeof(int));
    printf ("as a pointer v = %p\n",v);
    printf ("as an array v = ");
    vec_print (v,dim);

    /* copying a to v */
    for (int i=0;i<dim;i++) {
	v[i] = a[i];
    }
    printf ("after copy, v = ");
    vec_print (v,dim);

    /* free up the memory allocated for v */
    /* it is a good habit to set the pointer to 0 after calling free */
    free (v);
    v = 0;

    /* what will this code do? */
    //    printf ("after free, v = ");
    //    vec_print (v,dim);

}
