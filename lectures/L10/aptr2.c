#include <stdio.h>

int main () {
    int a[5] =  { 4, 9, 16, 25, 36 };
    printf ("a = { %d, %d, %d, %d %d }\n",a[0],a[1],a[2],a[3],a[4]);
    int* p = &(a[0]);
    printf ("p = %(a[0]) = %p\n",p);
    printf ("size of *p is %d\n",sizeof(*p));
    p = p + 2;
    printf ("p = p + 2 = %p\n",p);
    /* what will the following lines of code print? */
    printf ("*p = %d\n",*p);
    printf ("p[0] = %d\n",p[0]);
    printf ("p[1] = %d\n",p[1]);
    printf ("p[2] = %d\n",p[2]);
    printf ("p[-1] = %d\n",p[-1]);
    printf ("p[-2] = %d\n",p[-2]);
}
