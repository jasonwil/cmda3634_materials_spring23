#include <stdio.h>

int array_sum (int* v, int dim) {
    int sum = 0;
    for (int i=0;i<dim;i++) {
	sum += v[i];
    }
    return sum;
}

int main () {
    int a[5] =  { 4, 9, 16, 25, 36 };
    printf ("a = { %d, %d, %d, %d %d }\n",a[0],a[1],a[2],a[3],a[4]);
    int* p = &(a[0]);
    printf ("p = %(a[0]) = %p\n",p);
    /* when the variable a has an array type */
    /* then a is an abbreviation for &(a[0]) */
    p = a;
    printf ("p = a = %p\n",p);
    /* arrays are always passed as pointers in C */
    printf ("sum = %d\n",array_sum(a,5));
}
