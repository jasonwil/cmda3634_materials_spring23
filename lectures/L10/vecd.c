#include <stdio.h>
#include <stdlib.h>

/* how to implement a general vector in C? */

typedef struct vec3_s {
    double x;
    double y;
    double z;
} vec3_type;

#define VEC_SIZE 10
typedef struct vec_fixed_s {
    double data[VEC_SIZE];
} vec10_type;

#define MAX_VEC_SIZE 100000
typedef struct vec_max_s {
    double data[MAX_VEC_SIZE];
    int size;
} vec_max_type;

typedef struct vec_s {
    double* data;
    int size;
} vec_type;
