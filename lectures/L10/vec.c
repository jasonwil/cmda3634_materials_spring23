#include <stdio.h>
#include <stdlib.h>
#include "vec.h"

void vec_malloc (vec_type* v, int dim) {
    v->data = (double*)malloc(dim*sizeof(double));
    v->dim = dim;
}

void vec_calloc (vec_type* v, int dim) {
    v->data = (double*)calloc(dim,sizeof(double));
    v->dim = dim;
}

void vec_free (vec_type* v) {
    free (v->data);
    v->data = 0;
    v->dim = 0;
}

void vec_print (vec_type* v) {
    for (int i=0;i<v->dim;i++) {
	printf ("%g ",v->data[i]);
    }
    printf ("\n");
}

/* w = u + v */
void vec_add (vec_type* u, vec_type* v, vec_type* w) {
    for (int i=0;i<v->dim;i++) {
	w->data[i] = u->data[i] + v->data[i];
    }
}

/* w = cv */
void vec_mult (vec_type* v, double c, vec_type* w) {
    for (int i=0;i<v->dim;i++) {
	w->data[i] = v->data[i]*c;
    }
}

/* reads a vector from STDIN */
/* returns how many elements read */
int vec_read (vec_type* v) {
    int num_read = 0;
    for (int i=0;i<v->dim;i++) {
	if (scanf("%lf",&(v->data[i])) == 1) {
	    num_read += 1;
	}
    }
    return num_read;
}
