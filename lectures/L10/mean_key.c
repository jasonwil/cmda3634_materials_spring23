#include <stdio.h>
#include <stdlib.h>
#include "vec.h"

int main (int argc, char** argv) {

    int dim;
    if (scanf("%d",&dim) != 1) {
	printf ("error reading vector dimension\n");
	return 1;
    }

    vec_type mean,next;
    vec_calloc(&mean,dim);
    vec_malloc(&next,dim);

    int num = 0;
    while (vec_read(&next) == next.dim) {
        vec_add(&mean,&next,&mean);
        num += 1;
    }
    if (num < 1) {
        printf ("Error : Not enough points\n");
    }
    vec_mult(&mean,1.0/num,&mean); /* be careful why not use 1/num ?? */
    printf ("mean = ");
    vec_print (&mean);

    vec_free (&next);
    vec_free (&mean);

    return 0;
}
