#ifndef VEC_H
#define VEC_H

typedef struct vec_s {
    double* data;
    int dim;
} vec_type;

void vec_malloc (vec_type* v, int dim);

void vec_calloc (vec_type* v, int dim);

void vec_free (vec_type* v);

void vec_print (vec_type* v);

/* w = u + v */
void vec_add (vec_type* u, vec_type* v, vec_type* w);

/* w = cv */
void vec_mult (vec_type* v, double c, vec_type* w);

/* reads a vector from STDIN */
/* returns how many elements read */
int vec_read (vec_type* v);

#endif
