#ifndef VEC_H
#define VEC_H

typedef struct vec_s {
    double* data;
    int dim;
} vec_type;

/* initialize a vector to use a given data buffer of size dim */
/* vec_init must be called before any other vector operation */
void vec_init (vec_type* v, double* data, int dim);

/* print a vector using the provided format string */
void vec_print (vec_type* v, char* format);

/* w = u + v */
void vec_add (vec_type* u, vec_type* v, vec_type* w);

/* w = cv */
void vec_scalar_mult (vec_type* v, double c, vec_type* w);

/* reads a vector from STDIN */
/* returns how many elements read */
int vec_read_stdin (vec_type* v);

/* returns || v - w ||^2 */
double vec_dist_sq (vec_type* v, vec_type* w);

/* performs the deep copy v->data[i] = w->data[i] for all i */
void vec_copy (vec_type* v, vec_type* w);

/* zeros the vector v */
void vec_zero (vec_type* v);

#endif
