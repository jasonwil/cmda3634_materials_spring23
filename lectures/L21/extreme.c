#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mat.h"

typedef struct pair_s {
    double dist_sq;
    int i,j;
} pair_type;

pair_type find_extreme_pair (mat_type* dataset, int N) {
    pair_type extreme_pair = { 0, -1, -1 };
    vec_type row1, row2;
    for (int i = 0;i<N-1;i++) {
        for (int j = i+1;j<N;j++) {
            mat_get_row(dataset,&row1,i);
            mat_get_row(dataset,&row2,j);
            double dist_sq = vec_dist_sq(&row1,&row2);
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.i = i;
                extreme_pair.j = j;
            }
        }
    }
    return extreme_pair;
}

int main (int argc, char** argv) {

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int N = atoi(argv[1]);

    /* read in the mnist test set of 10000 images */
    int rows = 10000;
    int cols = 784;

    /* dynamically allocate memory for the (rows x cols) matrix */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix */
    mat_type dataset;
    mat_init (&dataset,data,rows,cols);

    /* read the binary file */
    matrix_read_bin(&dataset,"t10k-images-idx3-ubyte",16);

    /* test at most rows points */
    if (N > rows) {
        N = rows;
    }

    /* find the extreme pair in the first N points of the dataset */
    pair_type extreme_pair = find_extreme_pair (&dataset,N);

    /* output the results */
    printf ("Extreme Distance : %g\n",sqrt(extreme_pair.dist_sq));
    printf ("Extreme Indices : %d %d\n",extreme_pair.i,extreme_pair.j);

    /* free the dynamically allocated matrix data buffer */
    free (data);

}
