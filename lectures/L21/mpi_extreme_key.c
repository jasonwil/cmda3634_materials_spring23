#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "mat.h"

typedef struct pair_s {
    double dist_sq;
    int i,j;
} pair_type;

pair_type find_extreme_pair (mat_type* dataset, int N, int rank, int size) {
    pair_type extreme_pair = { 0, -1, -1 };
    vec_type row1, row2;
    for (int i = 0+rank;i<N-1;i+=size) {
        for (int j = i+1;j<N;j++) {
            mat_get_row(dataset,&row1,i);
            mat_get_row(dataset,&row2,j);
            double dist_sq = vec_dist_sq(&row1,&row2);
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.i = i;
                extreme_pair.j = j;
            }
        }
    }
    return extreme_pair;
}

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int N = atoi(argv[1]);

    /* read in the mnist test set of 10000 images */
    int rows = 10000;
    int cols = 784;

    /* dynamically allocate memory for the (rows x cols) matrix */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix */
    mat_type dataset;
    mat_init (&dataset,data,rows,cols);

    /* read the binary file */
    matrix_read_bin(&dataset,"t10k-images-idx3-ubyte",16);

    /* test at most rows points */
    if (N > rows) {
        N = rows;
    }

    /* start the timer */
    double start_time;
    start_time = MPI_Wtime();

    /* find the extreme pair in the first N points of the dataset */
    pair_type extreme_pair = find_extreme_pair (&dataset,N,rank,size);

    /* consolidate extreme pair info at root rank */
    int in_msg[2];
    int out_msg[2];
    MPI_Status status;
    int source, dest;

    if (rank == 0) {
        vec_type row1, row2;
        for (int i=1;i<size;i++) {
            source = i;
            MPI_Recv (in_msg, 2, MPI_INT, source, 0, MPI_COMM_WORLD,&status);
            mat_get_row(&dataset,&row1,in_msg[0]);
            mat_get_row(&dataset,&row2,in_msg[1]);
            double dist_sq = vec_dist_sq(&row1,&row2);
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.i = in_msg[0];
                extreme_pair.j = in_msg[1];
            }
        }
    } else {
        out_msg[0] = extreme_pair.i;
        out_msg[1] = extreme_pair.j;
        int dest = 0;
        MPI_Send (out_msg, 2, MPI_INT, dest, 0, MPI_COMM_WORLD);
    }

    /* stop the timer */
    double end_time;
    end_time = MPI_Wtime();

    /* output the results */
    if (rank == 0) {
        printf ("Extreme Distance : %g\n",sqrt(extreme_pair.dist_sq));
        printf ("Extreme Indices : %d %d\n",extreme_pair.i,extreme_pair.j);
        printf ("elapsed time = %g seconds\n",end_time-start_time);
    }

    /* free the dynamically allocated matrix data buffer */
    free (data);

    MPI_Finalize();
}
