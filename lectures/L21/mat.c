#include <stdio.h>
#include <stdlib.h>
#include "mat.h"

/* initialize a matrix to use a given data buffer of size rows*cols */
/* mat_init must be called before any other matrix operation */
void mat_init (mat_type* A, double* data, int rows, int cols) {
    A->data = data;
    A->rows = rows;
    A->cols = cols;
}

/* print a matrix using the provided format string */
void mat_print (mat_type* A, char* format) {
    for (int i=0;i<A->rows;i++) {
	for (int j=0;j<A->cols;j++) {
	    printf (format,A->data[j+i*A->cols]);
	}
	printf ("\n");
    }
}

/* C = A + B */
void mat_add (mat_type* A, mat_type* B, mat_type* C) {
    for (int i=0;i<A->rows*A->cols;i++) {
	C->data[i] = A->data[i] + B->data[i];
    }
}

/* B = cA */
void mat_scalar_mult (mat_type* A, double c, mat_type* B) {
    for (int i=0;i<A->rows*A->cols;i++) {
	B->data[i] = c*A->data[i];
    }
}

/* reads a matrix from STDIN */
/* returns how many elements read */
int mat_read_stdin (mat_type* A) {
    int num_read = 0;
    for (int i=0;i<A->rows*A->cols;i++) {
	if (scanf("%lf",&(A->data[i])) == 1) {
	    num_read += 1;
	}
    }
    return num_read;
}

/* returns || A - B ||^2_F */
/* The Frobenius norm squared of a matrix is the */
/* sum of the squares of all entries */
double mat_dist_sq (mat_type* A, mat_type* B) {
    double sum_sq = 0;
    for (int i=0;i<A->rows*A->cols;i++) {
	double diff = A->data[i] - B->data[i];
	sum_sq += diff*diff;
    }
    return sum_sq;
}

/* performs the deep copy A->data[i] = B->data[i] for all i */
void mat_copy (mat_type* A, mat_type* B) {
    for (int i=0;i<A->rows*A->cols;i++) {
	A->data[i] = B->data[i];
    }
}

/* zeros the matrix A */
void mat_zero (mat_type* A) {
    for (int i=0;i<A->rows*A->cols;i++) {
	A->data[i] = 0;
    }
}

/* initialize the vector row to the i^th row vector of A */
void mat_get_row (mat_type* A, vec_type* row, int i) {
    row->dim = A->cols;
    row->data = A->data+i*A->cols;
}

typedef unsigned char byte;

/* read a matrix from a binary file of unsigned chars */
void matrix_read_bin (mat_type* A, char* filename, int header_size) {

    byte header[header_size];
    FILE* fptr;

    /* open the binary file for reading */
    fptr = fopen(filename,"rb"); 

    /* need to check for null */
    if (fptr == 0) {
	printf ("Error opening binary data file.\n");
	exit(1);
    }

    /* allocate space for the data buffer */
    int data_size = A->rows*A->cols;
    byte* data = (byte*)malloc(data_size*sizeof(byte));
    if (data == 0) {
	printf ("Error allocating buffer to read data file.\n");
	exit(1);
    }

    /* skip over header */
    size_t num_read = 0;
    num_read = fread(header, sizeof(byte), header_size, fptr);

    /* read data */
    num_read = fread(data, sizeof(byte), data_size, fptr);

    /* store data in the given matrix */
    for (int i=0;i<A->rows;i++) {
	for (int j=0;j<A->cols;j++) {
	    A->data[i*A->cols+j] = (double)data[i*A->cols+j];
	}
    }

    /* free the data buffer */
    free (data);

    /* close the binary file */
    fclose(fptr);
}



