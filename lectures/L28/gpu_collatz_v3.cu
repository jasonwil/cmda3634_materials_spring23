#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>

typedef unsigned int uint32;
typedef unsigned long long int uint64;

/* compute the total stopping time for a given number n */
__device__ uint32 total_stopping_time (uint64 a_i) {
    uint32 total = 0;
    while (a_i != 1) {
        if (a_i % 2 == 0) {
            a_i = a_i/2;
        } else {
            a_i = (3*a_i+1);
        }
        total += 1;
    }
    return total;
}

__global__ void collatzKernel(uint64 N, uint64* d_starts, uint32* d_totals) {

    __shared__ uint64 max_start;
    __shared__ uint32 max_total;

    if (threadIdx.x == 0) {
        max_start = 1;
        max_total = 0;
    }
    __syncthreads();

    uint64 thread_num = (uint64)blockIdx.x*blockDim.x + threadIdx.x;
    uint32 total = 0;
    if (thread_num < N) {
        total = total_stopping_time(thread_num+1);
    }
    atomicMax(&max_total,total);
    __syncthreads();
    if (max_total == total) {
        atomicExch(&max_start,thread_num+1);
    }
    __syncthreads();

    if (threadIdx.x == 0) {
        d_starts[blockIdx.x] = max_start;
        d_totals[blockIdx.x] = max_total;
    }

}

int main (int argc, char** argv) {

    /* B is the number of threads per block */
    /* we typically choose B to be a multiple of 32 */
    /* the maximum value of B is 1024 */
    /* get N and B from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","B");
        return 1;
    }
    uint64 N = atol(argv[1]);
    int B = atoi(argv[2]);

    /* G is the number of thread blocks */
    /* the maximum number of thread blocks G is 2^31 - 1 = 2147483647 */
    /* We choose G to be the minimum number of thread blocks to have at least N threads */
    int G = (N+B-1)/B;
    printf ("N = %lu\n",N);
    printf ("threads per block B = %d\n",B); 
    printf ("number of thread blocks G = %d\n",G);
    printf ("number of threads G*B = %lu\n",(uint64)G*B);

    /* the starts and totals arrays in host memory */
    uint64* starts = (uint64*)malloc(G*sizeof(uint64));
    uint32* totals = (uint32*)malloc(G*sizeof(uint32));

    /* the totals array in device memory */
    uint64* d_starts;
    uint32* d_totals;
    cudaMalloc (&d_starts,G*sizeof(uint64));
    cudaMalloc (&d_totals,G*sizeof(uint32));

    /* launch kernels */
    collatzKernel <<< G, B >>> (N,d_starts,d_totals);

    /* copy total stopping time data from device to host */
    cudaMemcpy (starts, d_starts, G*sizeof(uint64),cudaMemcpyDeviceToHost);
    cudaMemcpy (totals, d_totals, G*sizeof(uint32),cudaMemcpyDeviceToHost);

    /* host finds the largest total stopping time */
    uint64 max_start = 1;
    uint32 max_total = 0;
    for (int b=0;b<G;b++) {
        if (totals[b] > max_total) {
            max_start = starts[b];
            max_total = totals[b];
        }
    }

    /* output the results */
    printf ("The starting value less than or equal to %lu\n",N);
    printf ("  having the largest total stopping time is %lu\n",max_start);
    printf ("  which has %u steps\n",max_total);

    /* free the memory on the host */
    free (totals);

    /* free the memory on the device */
    cudaFree (d_totals);
}
