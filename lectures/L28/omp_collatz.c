#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned int uint32;
typedef unsigned long long int uint64;

/* compute the total stopping time for a given number n */
uint32 total_stopping_time (uint64 a_i) {
    uint32 total = 0;
    while (a_i != 1) {
        if (a_i % 2 == 0) {
            a_i = a_i/2;
        } else {
            a_i = (3*a_i+1);
        }
        total += 1;
    }
    return total;
}

int main(int argc, char **argv) {

    /* get N from command line */
    if (argc != 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    uint64 N = (uint64)atol(argv[1]);

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    uint64 max_total[2];
    max_total[0] = 1;
    max_total[1] = 0;

#pragma omp parallel default(none) shared(max_total,N)
    {
        uint64 max_total_thread[2];
        max_total_thread[0] = 1;
        max_total_thread[1] = 0;
#pragma omp for schedule(dynamic,1000)
        for (uint64 n = 1;n<=N;n++) {
            uint64 total = total_stopping_time(n);
            if (total > max_total_thread[1]) {
                max_total_thread[0] = n;
                max_total_thread[1] = total;
            }
        }
#pragma omp critical 
        {
            if (max_total_thread[1] > max_total[1]) {
                max_total[0] = max_total_thread[0];
                max_total[1] = max_total_thread[1];
            }   
        }
    }

    /* stop the timer */
    end_time = omp_get_wtime();

    /* output the results */
    printf ("The starting value less than or equal to %lu\n",N);
    printf ("  having the largest total stopping time is %lu\n",max_total[0]);
    printf ("  which has %lu steps\n",max_total[1]);
    printf ("wall time used = %g sec\n",end_time-start_time);

    return 0;
}



