import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 4):
    print ("Command Usage : python3",sys.argv[0],"datafile","imagefile","k")
    exit(1)
datafile = sys.argv[1]
imagefile = sys.argv[2]
k = int(sys.argv[3])

# read the data file
data = np.loadtxt(datafile,skiprows=1)

# pick a random subset of points to start with
np.random.seed(0)
sample = np.random.choice(np.arange(len(data)),k,replace=False)
kmeans = data[sample]

for iter in range(10):

    # determine the cluster for each point
    clusters = np.zeros(len(data),dtype='int')
    distances_sq = np.zeros(len(kmeans))
    for i in range(len(data)):
        for j in range(len(kmeans)):
            distances_sq[j] = np.inner(data[i]-kmeans[j],data[i]-kmeans[j])
        clusters[i] = np.argmin(distances_sq)

    # update the kmeans
    kmeans_next = np.zeros(kmeans.shape)
    cluster_sizes = np.zeros(len(kmeans),dtype='int')
    for i in range(len(data)):
        cluster_sizes[clusters[i]] += 1
        kmeans_next[clusters[i]] += data[i]
    for i in range(len(kmeans)):
        kmeans_next[i] /= cluster_sizes[i]

    np.copyto(kmeans,kmeans_next)

# plot the points arranged into clusters and the kmeans
plt.gca().set_aspect('equal')
plt.scatter (data[:,0],data[:,1],c=clusters,cmap="tab10",s=1)
plt.scatter (kmeans[:,0],kmeans[:,1],c=np.arange(len(kmeans)),cmap="tab10",s=100)
plt.scatter (kmeans[:,0],kmeans[:,1],facecolors='none', edgecolors='black',s=100)

# save the plot as an image
plt.savefig(imagefile)

