#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --cpus-per-task=4
#SBATCH -o omp_extreme.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# compile the C program with OpenMP enabled 
gcc -o omp_extreme omp_extreme.c mat.c vec.c -lm -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=spread
export OMP_PLACES=cores

# run omp_sum
cat $1 | ./omp_extreme 1
cat $1 | ./omp_extreme 2
cat $1 | ./omp_extreme 4

