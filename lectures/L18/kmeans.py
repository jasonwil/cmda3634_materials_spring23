import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 4):
    print ("Command Usage : python3",sys.argv[0],"datafile","imagefile","k")
    exit(1)
datafile = sys.argv[1]
imagefile = sys.argv[2]
k = int(sys.argv[3])

# read the data file
data = np.loadtxt(datafile,skiprows=1)

# plot the data
plt.scatter(data[:,0],data[:,1],s=1,color='black')

# pick a random subset of points to start with
np.random.seed(0)
sample = np.random.choice(np.arange(len(data)),k,replace=False)
kmeans = data[sample]

# plot the points arranged into clusters and the kmeans
plt.gca().set_aspect('equal')
plt.scatter (data[:,0],data[:,1],s=1,color='orange')
plt.scatter (kmeans[:,0],kmeans[:,1],c=np.arange(len(kmeans)),cmap="tab10",s=100)
plt.scatter (kmeans[:,0],kmeans[:,1],c=np.arange(len(kmeans)),cmap="tab10",s=100,
             facecolors='none', edgecolors='black')

# save the plot as an image
plt.savefig(imagefile)

