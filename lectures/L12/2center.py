import sys
import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt(sys.stdin)

def distance_sq(p1,p2):
    return np.inner(p1-p2,p1-p2)

def centers_cost(data,c1,c2):
    cost_sq = 0
    num = len(data)
    for i in range(num):
        dist_sq_1 = distance_sq(data[i],data[c1])
        dist_sq_2 = distance_sq(data[i],data[c2])
        min_dist_sq = dist_sq_1
        if (dist_sq_2 < min_dist_sq):
            min_dist_sq = dist_sq_2
        if (min_dist_sq > cost_sq):
            cost_sq = min_dist_sq
            extreme = i
    return (np.sqrt(cost_sq),extreme)

#print (centers_cost(data,3,4))

def solve_2center(data,centers):
    min_cost = float("inf")
    num = len(data)
    for i in range(0,num-1):
        for j in range(i+1,num):
            cost,extreme = centers_cost(data,i,j)
            if (cost < min_cost):
                min_cost = cost
                centers[0] = i
                centers[1] = j

centers = np.zeros(2,dtype='int')
solve_2center(data,centers)
print (centers)
c1 = centers[0]
c2 = centers[1]

#p1 = np.array([1,2])
#p2 = np.array([3,4])
#print (distance_sq(p1,p2))

cost,extreme = centers_cost(data,c1,c2)
print ('cost =',cost)

plt.gca().set_aspect('equal')
plt.scatter (data[:,0],data[:,1],s=10,color='black')
plt.scatter (data[c1,0],data[c1,1],s=100,color='orange')
plt.scatter (data[c2,0],data[c2,1],s=100,color='maroon')
plt.scatter (data[extreme,0],data[extreme,1],s=50,facecolors='none',
        edgecolors='black')
plt.savefig('2center.png')
