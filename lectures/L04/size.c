#include <stdio.h>

int main () {
    printf ("A C char requires %ld bytes of storage\n",sizeof(char));
    printf ("A C short requires %ld bytes of storage\n",sizeof(short));
    printf ("A C int requires %ld bytes of storage\n",sizeof(int));
    printf ("A C long int requires %ld bytes of storage\n",sizeof(long));
    printf ("A C long long int requires %ld bytes of storage\n",sizeof(long long));
    printf ("A C float requires %ld bytes of storage\n",sizeof(float));
    printf ("A C double requires %ld bytes of storage\n",sizeof(double));
    printf ("A C long double requires %ld bytes of storage\n",sizeof(long double));
}
