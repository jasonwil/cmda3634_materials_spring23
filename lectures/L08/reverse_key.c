#include <stdio.h>
#include "stack_key.h"

int main () {
    stack_type stack;
    stack_init (&stack);
    int top;
    int N = 10000;
    for (int i=1;i<=N;i++) {
        stack_push (&stack,i);
    }
    for (int i=1;i<=N;i++) {
        top = stack_top (&stack);
        printf ("%d\n",top);
        stack_pop (&stack);
    }
}
