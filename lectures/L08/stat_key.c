#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_NUM_SCORES 1000

int main () {

    int num_scores = 0;
    float scores[MAX_NUM_SCORES];
    float score;
    while (scanf("%f",&score) == 1) {
        if (num_scores < MAX_NUM_SCORES) {
            scores[num_scores++] = score;
        }
    }

    if (num_scores < 2) {
        printf ("Not enough scores!\n");
        exit(1);
    }

    float sum = 0;
    for (int i=0;i<num_scores;i++) {
        sum += scores[i];
    }

    float sample_mean = sum/num_scores;
    printf ("Average =  %.2f\n",sample_mean);

    float sum_diff_sq = 0;
    for (int i=0;i<num_scores;i++) {
        float temp = scores[i]-sample_mean;
        sum_diff_sq += temp*temp;
    }
    float sample_variance = sum_diff_sq/(num_scores-1);

    float sample_stdev = sqrt(sample_variance);
    printf ("Standard Deviation = %.2f\n",sample_stdev);

    int done = 0;
    while (!done) {
        done = 1;
        for (int i=0;i<num_scores-1;i++) {
            if (scores[i] > scores[i+1]) {
                float temp = scores[i];
                scores[i] = scores[i+1];
                scores[i+1] = temp;
                done = 0;
            }
        }
    }

    float median;
    if (num_scores % 2 == 1) {
        median = scores[num_scores/2];
    } else {
        median = (scores[num_scores/2-1]+scores[num_scores/2])/2.0;
    }
    printf ("Median = %.2f\n",median);

    int num_As = 0;
    int num_Bs = 0;
    int num_Cs = 0;
    int num_Ds = 0;
    int num_Fs = 0;
    
    for (int i=0;i<num_scores;i++) {
        if (scores[i] < 60.0) {
            num_Fs += 1;
        } else if (scores[i] < 70.0) {
            num_Ds += 1;
        } else if (scores[i] < 80.0) {
            num_Cs += 1;
        } else if (scores[i] < 90.0) {
            num_Bs += 1;
        } else {
            num_As += 1;
        }
    }

    printf ("Number of A's = %d\n",num_As);
    printf ("Number of B's = %d\n",num_Bs);
    printf ("Number of C's = %d\n",num_Cs);
    printf ("Number of D's = %d\n",num_Ds);
    printf ("Number of F's = %d\n",num_Fs);

    return 0;
}
