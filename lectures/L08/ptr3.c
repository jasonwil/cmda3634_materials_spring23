#include <stdio.h>
#include "stack.h"

int main () {
    stack_type stack;
    printf ("The size of stack is %ld bytes\n",sizeof(stack));
    printf ("The address of the stack is %p\n",&stack);
    printf ("The size of the address of the stack is %ld bytes\n",
            sizeof(&stack));
}
