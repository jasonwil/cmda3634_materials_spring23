#include <stdio.h>

int main () {
    int a = 5;
    printf ("the value of a is %d\n",a);
    printf ("the size of a is %ld bytes\n",sizeof(a));
    printf ("the address of a is %p\n",&a);
    printf ("the size of the address of a is %ld bytes\n",sizeof(&a));
}
