#include <stdio.h>
#include "vec2.h"

int main () {
    int num_points = 0;
    vec2_type mean = { 0, 0 };
    vec2_type next;
    while (scanf("%lf %lf",&(next.x),&(next.y)) == 2) {
        mean = vec2_add (mean,next);
        num_points += 1;
    }   
    mean = vec2_scalar_mult (mean, 1.0/num_points);
    printf ("mean of data = ");
    vec2_print (mean);
}
