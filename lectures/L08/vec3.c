#include <stdio.h>

typedef struct vec3_s {
    double x, y, z;
} vec3_type;

void vec3_scalar_mult_v1 (vec3_type v, double c) {
    v.x *= c;
    v.y *= c;
    v.z *= c;
}

vec3_type vec3_scalar_mult_v2 (vec3_type v, double c) {
    v.x *= c;
    v.y *= c;
    v.z *= c;
    return v;
}

void vec3_scalar_mult_v3 (vec3_type* v, double c) {
    v->x *= c;
    v->y *= c;
    v->z *= c;
}

int main () {
    vec3_type v = { 1, 2, 3 };
    printf ("initial v is  { %g, %g, %g }\n",v.x,v.y,v.z);
    vec3_scalar_mult_v1 (v,2);
    printf ("v after v1 is { %g, %g, %g }\n",v.x,v.y,v.z);
    v = vec3_scalar_mult_v2 (v,2);
    printf ("v after v2 is { %g, %g, %g }\n",v.x,v.y,v.z);
    vec3_scalar_mult_v3 (&v,2);
    printf ("v after v3 is { %g, %g, %g }\n",v.x,v.y,v.z);
}

