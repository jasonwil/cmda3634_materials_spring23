#include <stdio.h>
#include <float.h>

int main () {
    float min = FLT_MAX;
    float number;
    while (scanf("%f",&number) == 1) {
        if (number < min) {
            min = number;
        }
    }
    printf ("The smallest number is %g\n",min);
}
