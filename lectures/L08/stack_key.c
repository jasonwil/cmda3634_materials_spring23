#include <stdio.h>
#include <stdlib.h>
#include "stack_key.h"

/* initializes a stack to be empty */
void stack_init (stack_type* stack) {
    stack->size = 0;
}

/* returns the top element of the stack */
/* exits with an error if the stack is empty */
int stack_top (stack_type* stack) {
    if (stack->size == 0) {
        printf ("stack underflow!\n");
        exit(1);
    }
    return (stack->data[stack->size-1]);
}

/* pushes the given element onto the top of the stack */
/* exits with an error if the stack is full */
void stack_push (stack_type* stack, int element) {
    if (stack->size == MAX_STACK_SIZE) {
        printf ("stack overflow!\n");
        exit(1);
    }
    stack->data[stack->size++] = element;
}

/* pops the top element off of the stack */
/* exits with an error if the stack is empty */
void stack_pop (stack_type* stack) {
    if (stack->size == 0) {
        printf ("stack underflow!\n");
        exit(1);
    }
    stack->size--;
}

