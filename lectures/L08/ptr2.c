#include <stdio.h>

int main () {
    int a = 5;
    printf ("the value of a is %d\n",a);
    int* p = &a;
    printf ("the value of p is %p\n",p);
    *p = 6;
    printf ("the new value of a is %d\n",a);
}
