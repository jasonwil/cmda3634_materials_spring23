#include <stdio.h>
#include "stack.h"

int main () {
    stack_type stack;
    stack = stack_init (stack);
    int top;
    int N = 10;
    for (int i=1;i<=N;i++) {
        stack = stack_push (stack,i);
    }
    for (int i=1;i<=N;i++) {
        top = stack_top (stack);
        printf ("%d\n",top);
        stack = stack_pop (stack);
    }
}
