#include <stdio.h>
#include <stdlib.h>

void add1_v1 (int n) {
    n = n + 1;
}

int add1_v2 (int n) {
    return n + 1;
}

void add1_v3 (int* n) {
    n = n + 1; 
}

void add1_v4 (int* n) {
    *n = *n + 1; 
}

int main () {
    int a = 3;
    printf ("initially:     a = %d\n",a);
    add1_v1(a);
    printf ("after add1_v1: a = %d\n",a);
    a = add1_v2(a);
    printf ("after add1_v2: a = %d\n",a);  
    add1_v3(&a);
    printf ("after add1_v3: a = %d\n",a);
    add1_v4(&a);
    printf ("after add1_v4: a = %d\n",a);    
}
