#include <stdio.h>
#include "vec2.h"

int main () {
    vec2_type v = { 1, 2 };
    printf ("the value of v is {%g, %g}\n",v.x,v.y);
    vec2_type* p = &v;
    printf ("the value of p is %p\n",p);
    (*p).x = 3;
    p->y = 4;
    printf ("the new value of v is {%g, %g}\n",v.x,v.y);
}
