#include <stdio.h>
#include <stdlib.h>
#include "vec.h"

int main (int argc, char** argv) {

    /* read the vector dimension */
    int dim;
    if (scanf("%d",&dim) != 1) {
	printf ("error reading vector dimension\n");
	return 1;
    }

    /* dynamically allocate space for the mean and next vectors */
    double* mean_data = (double*)malloc(dim*sizeof(double));
    double* next_data = (double*)malloc(dim*sizeof(double));
    vec_type mean,next;
    vec_init(&mean,mean_data,dim);
    vec_init(&next,next_data,dim);

    /* set elements of the mean vector to 0 */
    /* how could you combine this step with a previous step? */
    vec_zero(&mean);

    /* compute the mean of the data */
    int num = 0;
    while (vec_read_stdin(&next) == dim) {
        vec_add(&mean,&next,&mean);
        num += 1;
    }
    if (num < 1) {
        printf ("Error : Not enough points\n");
    }
    /* be careful why not use 1/num ?? */
    vec_scalar_mult(&mean,1.0/num,&mean);
    printf ("mean = ");
    vec_print (&mean,"%.1lf ");

    /* free the dynamically allocated buffers */
    free (mean_data);
    free (next_data);

    return 0;
}
