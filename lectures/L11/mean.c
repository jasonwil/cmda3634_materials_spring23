#include <stdio.h>
#include <stdlib.h>
#include "vec.h"

int main (int argc, char** argv) {

    /* read the vector dimension */
    int dim;
    if (scanf("%d",&dim) != 1) {
	printf ("error reading vector dimension\n");
	return 1;
    }

    /* dynamically allocate space for the mean and next vectors */

    /* set elements of the mean vector to 0 */
    /* how could you combine this step with a previous step? */

    /* compute the mean of the data */

    /* free the dynamically allocated buffers */

    return 0;
}
