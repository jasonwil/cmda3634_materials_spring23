#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mat.h"

typedef struct pair_s {
    double dist_sq;
    int i,j;
} pair_type;

pair_type find_extreme_pair (mat_type* A) {
    pair_type extreme_pair = { 0, -1, -1 };
    vec_type row1, row2;
    for (int i = 0;i<A->rows-1;i++) {
        for (int j = i+1;j<A->rows;j++) {
	    /******************************************************/
	    /* calculate the distance squared between row i and j */
            double dist_sq;
	    /******************************************************/
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.i = i;
                extreme_pair.j = j;
            }
        }
    }
    return extreme_pair;
}

int main (int argc, char** argv) {

    /* read the shape of the matrix from STDIN */
    int rows, cols;
    if (scanf("%d %d",&rows,&cols) != 2) {
	printf ("error reading the shape of the matrix\n");
	return 1;
    }

    /* dynamically allocate memory for the (rows x cols) matrix */

    /* initialize the matrix */

    /* read the matrix from STDIN */

    /* find the extreme pair */
    pair_type extreme_pair = find_extreme_pair (&A);

    /* output the results */
    printf ("Extreme Distance : %g\n",sqrt(extreme_pair.dist_sq));
    printf ("Extreme Indices : %d %d\n",extreme_pair.i,extreme_pair.j);

    /* free the dynamically allocated matrix data buffer */

    return 0;
}
