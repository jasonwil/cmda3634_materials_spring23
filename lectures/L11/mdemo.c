#include <stdio.h>
#include <stdlib.h>
#include "mat.h"

int main () {
    double A_data[3*3] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    double B_data[3*3] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    double C_data[3*3];
    double D_data[3*3];
    mat_type A, B, C, D;
    mat_init (&A,A_data,3,3);
    mat_init (&B,B_data,3,3);
    mat_init (&C,C_data,3,3);
    mat_init (&D,D_data,3,3);
    printf ("A = \n");
    mat_print(&A,"%g ");
    printf ("B = \n");
    mat_print(&B,"%g ");
    mat_add(&A,&B,&C);
    printf ("A + B = \n");
    mat_print(&C,"%g ");
    mat_scalar_mult(&C,2.0,&D);
    printf ("2(A + B) = \n");
    mat_print(&D,"%g ");
}
