#include <stdio.h>
#include <stdlib.h>
#include "mat.h"

int main () {
    double v_data[9] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    double w_data[9] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    double x_data[9];
    double y_data[9];
    vec_type v,w,x,y;
    vec_init (&v,v_data,9);
    vec_init (&w,w_data,9);
    vec_init (&x,x_data,9);
    vec_init (&y,y_data,9);
    printf ("v = ");
    vec_print(&v,"%g ");
    printf ("w = ");
    vec_print(&w,"%g ");
    vec_add(&v,&w,&x);
    printf ("v + w = ");
    vec_print(&x,"%g ");
    vec_scalar_mult(&x,2,&y);
    printf ("2(v + w) = ");
    vec_print(&y,"%g ");
}
