#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long long uint64;

int main(int argc, char **argv) {

    /* get N and thread_count from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","thread_count");
        return 1;
    }

    uint64 N = atol(argv[1]);
    int thread_count = atoi(argv[2]);

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    uint64 sum = 0;
#pragma omp parallel default(none) shared(N,sum) num_threads(thread_count)
    {
#pragma omp for
        for (uint64 i = 1; i <= N;i++) {
#pragma omp critical
	    {
		sum += i;
	    }
        }
    }   

    /* stop the timer */
    end_time = omp_get_wtime();

    printf ("thread_count = %d, ",thread_count);
    printf ("elapsed time = %g\n",end_time-start_time);
    printf (" N = %lu, ",N);
    printf ("sum = %lu, ",sum);
    printf ("N*(N+1)/2 = %lu\n",(N/2)*(N+1));
}
