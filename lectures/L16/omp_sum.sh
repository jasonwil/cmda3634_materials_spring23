#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --cpus-per-task=4
#SBATCH -o omp_sum.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# compile the C program with OpenMP enabled 
gcc -o omp_sum omp_sum.c -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=spread

# run omp_sum
./omp_sum $1 1
./omp_sum $1 2
./omp_sum $1 4

