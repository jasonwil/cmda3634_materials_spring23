#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --cpus-per-task=128
#SBATCH -o omp_sum_study.out
#SBATCH --exclusive

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# compile the C program with OpenMP enabled 
gcc -D STUDY -o omp_sum omp_sum.c -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=spread

# run omp_sum
./omp_sum 4000000000 1
./omp_sum 4000000000 2
./omp_sum 4000000000 4
./omp_sum 4000000000 8
./omp_sum 4000000000 16
./omp_sum 4000000000 32
./omp_sum 4000000000 64
./omp_sum 4000000000 128


