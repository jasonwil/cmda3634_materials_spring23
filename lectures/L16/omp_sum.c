#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long long uint64;

int main(int argc, char **argv) {

    /* get N and thread_count from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","thread_count");
        return 1;
    }

    uint64 N = atol(argv[1]);
    int thread_count = atoi(argv[2]);

    uint64 sum = 0;
#pragma omp parallel num_threads(thread_count)
    {
#pragma omp for
        for (uint64 i = 1; i <= N;i++) {
	    sum += i;
        }
    }   

    printf ("thread_count = %d\n",thread_count);
    printf (" N = %lu, ",N);
    printf ("sum = %lu, ",sum);
    printf ("N*(N+1)/2 = %lu\n",(N/2)*(N+1));
}
