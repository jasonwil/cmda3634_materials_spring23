#include <stdio.h>
#include <stdlib.h> 
#include "minheap.h"

int main () {
  minheap_type heap;
  heap_element_type next;
  minheap_init (&heap);

  /* read the numbers from standard input and insert into heap */
  while (scanf("%lf",&(next.value)) == 1) {
      minheap_insert(&heap,next);
  }

  /* print out the numbers in sorted order */
  while (minheap_size(&heap) > 0) {
      printf ("%g\n",minheap_top(&heap).value);
      minheap_remove_top(&heap);
  }
}
