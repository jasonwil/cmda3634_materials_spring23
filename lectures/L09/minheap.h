#ifndef MINHEAP_H
#define MINHEAP_H 1

#define MAX_HEAP_SIZE 1000000

/* heap element type */
typedef struct heap_element_s {
    double value;
    int index;
} heap_element_type;

/* minheap structure */
typedef struct minheap_s {
    heap_element_type nodes[MAX_HEAP_SIZE];
    int size;
} minheap_type;

/* initialize heap */
void minheap_init(minheap_type* heap);

/* return size of heap */
int minheap_size(minheap_type* heap);

/* return element on top of heap */
heap_element_type minheap_top(minheap_type* heap);

/* insert an element into heap */
void minheap_insert(minheap_type* heap, heap_element_type new);

/* remove element on top of heap */
void minheap_remove_top(minheap_type* heap);

#endif
