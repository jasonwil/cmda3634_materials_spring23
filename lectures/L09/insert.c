#include <stdio.h>
#include <stdlib.h>

#define MAX_NUM 1000000

int main () {

    /* read the numbers from standard input */
    int n = 0;
    double numbers[MAX_NUM];
    double next;
    while (scanf ("%lf",&next) == 1) {
	if (n == MAX_NUM) {
	    printf ("Too many numbers!\n");
	    exit(1);
	}
	numbers[n++] = next;
    }

    /* Insertion sort */
    for (int i=0;i<n;i++) {
	int cur = i;
	int done = 0;
	int next = numbers[i];
	while (cur > 0 && !done) {
	    if (numbers[cur-1] > next) {
		numbers[cur] = numbers[cur-1];
		cur -= 1;
	    } else {
		done = 1;
	    }
	}
	numbers[cur] = next;
    }

    /* print the result one number per line */
    for (int i=0;i<n;i++) {
	printf ("%g\n",numbers[i]);
    }
}
