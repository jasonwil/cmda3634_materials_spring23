#ifndef VEC2_H
#define VEC2_H

typedef struct vec2_s {
    double x, y;
} vec2_type;

void vec2_print (vec2_type v);

vec2_type vec2_add (vec2_type v, vec2_type w);

vec2_type vec2_scalar_mult (vec2_type v, double c);

double vec2_dist_sq (vec2_type v, vec2_type w);

double vec2_len_sq (vec2_type v);

#endif
