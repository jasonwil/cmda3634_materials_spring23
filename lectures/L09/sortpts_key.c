#include <stdio.h>
#include <math.h>
#include "vec2.h"
#include "minheap.h"

int main () {
    minheap_type heap;
    heap_element_type next;
    vec2_type pt;
    int num_pts = 0;
    minheap_init (&heap);

    while (scanf("%lf %lf",&(pt.x),&(pt.y)) == 2) {
	next.value = pt.x*pt.x+pt.y*pt.y;
	next.index = num_pts++;
	minheap_insert(&heap,next);
    }   

  /* print out the numbers in sorted order */
  while (minheap_size(&heap) > 0) {
      next = minheap_top(&heap);
      printf ("%d (%g)\n",next.index,sqrt(next.value));
      minheap_remove_top(&heap);
  }

}
