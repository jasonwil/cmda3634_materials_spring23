#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>

typedef unsigned long int uint64;
typedef unsigned __int128 uint128;

/* compute the total stopping time for a given number n */
uint64 total_stopping_time (uint64 n) {
    uint128 a_i = (uint128)n;
    uint64 total = 0;
    while (a_i != 1) {
	if (a_i % 2 == 0) {
	    a_i = a_i/2;
	} else {
	    a_i = (3*a_i+1);
	}
	total += 1;
    }
    return total;
}

int main(int argc, char **argv) {

    MPI_Init (&argc, &argv);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* get N from command line */
    if (argc != 2) {
	printf ("Command usage : %s %s\n",argv[0],"N");
	return 1;
    }
    uint64 N = (uint64)atol(argv[1]);
    if (N % size != 0) {
	printf ("error : N must be a multiple of size\n");
	exit(1);
    }

    uint64 max_total[2];
    max_total[0] = 1;
    max_total[1] = 0;

#pragma omp parallel default(none) shared(max_total,N,rank,size)
    {
	uint64 thread_max_total[2];
	thread_max_total[0] = 1;
        thread_max_total[1] = 0;
#pragma omp for schedule(dynamic,1000)
	for (uint64 n = 1+rank;n<=N;n+=size) {
	    uint64 total = total_stopping_time(n);
	    if (total > thread_max_total[1]) {
		thread_max_total[0] = n;
		thread_max_total[1] = total;
	    }
	}
#pragma omp critical 
	{
	    if (thread_max_total[1] > max_total[1]) {
		max_total[0] = thread_max_total[0];
		max_total[1] = thread_max_total[1];
	    }   
	}
    }

    uint64 max_totals[2*size];
    MPI_Gather (max_total,2,MPI_LONG_LONG_INT,max_totals,2,
		MPI_LONG_LONG_INT,0,MPI_COMM_WORLD);
    if (rank == 0) {
	for (int i=1;i<size;i++) {
	    if (max_totals[2*i+1] > max_total[1]) {
		max_total[0] = max_totals[2*i];
		max_total[1] = max_totals[2*i+1];
	    }
	}
    }

    /* stop timer */
    end_time = MPI_Wtime();

    if (rank == 0) {

	/* print wall time */
	printf ("wall time used = %.4f sec\n",(end_time-start_time));

	/* output the results */
	printf ("The starting value less than or equal to %lu\n",N);
	printf ("  having the largest total stopping time is %lu\n",max_total[0]);
	printf ("  which has %lu steps\n",max_total[1]);
    }
}



