#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "mat.h"

typedef struct pair_s {
    double dist_sq;
    int pair[2];
} pair_type;

pair_type find_extreme_pair (mat_type* dataset, int N, int rank, int size) {
    pair_type extreme_pair = { 0, { -1, -1 } };
    vec_type row1, row2;
    for (int i = 0+rank;i<N-1;i+=size) {
        for (int j = i+1;j<N;j++) {
            mat_get_row(dataset,&row1,i);
            mat_get_row(dataset,&row2,j);
            double dist_sq = vec_dist_sq(&row1,&row2);
            if (dist_sq > extreme_pair.dist_sq) {
                extreme_pair.dist_sq = dist_sq;
                extreme_pair.pair[0] = i;
                extreme_pair.pair[1] = j;
            }
        }
    }
    return extreme_pair;
}

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int N = atoi(argv[1]);

    /* read in the mnist test set of 10000 images */
    int rows = 10000;
    int cols = 784;

    /* dynamically allocate memory for the (rows x cols) matrix */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix */
    mat_type dataset;
    mat_init (&dataset,data,rows,cols);

    /* read the binary file */
    matrix_read_bin(&dataset,"t10k-images-idx3-ubyte",16);

    /* test at most rows points */
    if (N > rows) {
        N = rows;
    }

    /* start the timer */
    double start_time;
    start_time = MPI_Wtime();

    /* find the extreme pair in the first N points of the dataset */
    pair_type extreme_pair = find_extreme_pair (&dataset,N,rank,size);

    /* create the datatype */
    MPI_Datatype MPI_pair_type;
    int lengths[2] = { 1, 2 };
    MPI_Aint addresses[3];
    MPI_Aint displacements[2];
    MPI_Datatype types[2] = { MPI_DOUBLE, MPI_INT };
    MPI_Get_address (&extreme_pair,&addresses[0]);
    MPI_Get_address (&extreme_pair.dist_sq, &addresses[1]);
    MPI_Get_address (&extreme_pair.pair, &addresses[2]);
    displacements[0] = MPI_Aint_diff(addresses[1], addresses[0]);
    displacements[1] = MPI_Aint_diff(addresses[2], addresses[0]);
    MPI_Type_create_struct(2, lengths, displacements, types, &MPI_pair_type);
    MPI_Type_commit(&MPI_pair_type);

    /* gather the extreme pairs onto rank 0 */
    pair_type extreme_pairs[size];
    MPI_Gather (&extreme_pair,1,MPI_pair_type,extreme_pairs,1,MPI_pair_type,0,MPI_COMM_WORLD);

    /* rank 0 finds the extreme pair */
    if (rank == 0) {
        for (int i=1;i<size;i++) {
            if (extreme_pairs[i].dist_sq > extreme_pair.dist_sq) {
                extreme_pair = extreme_pairs[i];
            }
        }
    }

    /* stop the timer */
    double end_time;
    end_time = MPI_Wtime();

    /* rank 0 outputs the results */
    if (rank == 0) {
        printf ("Extreme Distance : %g\n",sqrt(extreme_pair.dist_sq));
        printf ("Extreme Indices : %d %d\n",extreme_pair.pair[0],extreme_pair.pair[1]);
        printf ("elapsed time = %g seconds\n",end_time-start_time);
    }

    /* free the dynamically allocated matrix data buffer */
    free (data);

    MPI_Finalize();
}
