#include <stdio.h>

typedef struct test_s {
    int a;
    double b;
} test_type;

int main () {
    test_type test;
    printf ("%p %p %p\n",&test,&test.a,&test.b);
    printf ("sizeof (test) = %ld\n",sizeof(test));
    printf ("sizeof (test.a) = %ld\n",sizeof(test.a));
    printf ("sizeof (test.b) = %ld\n",sizeof(test.b));
}
