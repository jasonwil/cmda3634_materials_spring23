#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=64
#SBATCH -o mpi_omp_collatz.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib > /dev/null

# Build the executable
mpicc -O3 -o mpi_omp_collatz mpi_omp_collatz.c -fopenmp

# run mpi_omp_collatz
mpiexec -np $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node:PE=$SLURM_CPUS_PER_TASK -x OMP_PROC_BIND=true ./mpi_omp_collatz $1
