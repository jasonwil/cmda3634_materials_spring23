#include <stdio.h>
#include <float.h>

#define N 10

int main () {
    float num[N] = { -2.3, 7.2, -4.5, 1.6, 2.3, 
        -0.7, 5.3, 0.9, 3.4, 6.1 };
    float min = FLT_MAX;
    float next = FLT_MAX;
    printf ("The smallest number is %g\n",min);
    printf ("The next smallest number is %g\n",next);
}
