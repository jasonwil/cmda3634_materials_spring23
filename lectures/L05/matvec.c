#include <stdio.h>

#define N 5

int main () {
    int A[N][N] = { { 1, 2, 3, 4, 5 }, { 2, 3, 4, 5, 1}, {3, 4, 5, 1, 2},
        { 4, 5, 1, 2, 3 }, { 5, 1, 2, 3, 4} };
    int x[N] = { 1, 2, 3, 2, 1};
    int Ax[N];

    for (int i=0;i<N;i++) {
        Ax[i] = 0;
    }

    printf ("Ax = ");
    for (int i=0;i<N;i++) {
        printf ("%d ",Ax[i]);
    }
    printf ("\n");
}
