#include <stdio.h>

#define N 10

int main () {
    float num[N] = { -2.3, 7.2, -4.5, 1.6, 2.3, 
        -0.7, 5.3, 0.9, 3.4, 6.1 };

    int done = 0;
    while (!done) {
        done = 1;
        for (int i = 0;i < N-1 ;i++) {
            if (num[i] > num[i+1]) {
                float temp = num[i+1];
                num[i+1] = num[i];
                num[i] = temp;
                done = 0;
            }
        }
    }

    printf ("num = ");
    for (int i=0;i<N;i++) {
        printf ("%g ",num[i]);
    }
    printf ("\n");
}
