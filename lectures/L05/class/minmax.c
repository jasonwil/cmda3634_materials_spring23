#include <stdio.h>
#include <float.h>

#define N 10

int main () {
    float num[N] = { -2.3, 7.2, -4.5, 1.6, 2.3, 
        -0.7, 5.3, 0.9, 3.4, 6.1 };
    float min = FLT_MAX;
    float max = FLT_MIN;
    for (int i = 0; i < N; i++) {
        if (num[i] < min) {
            min = num[i];
        }
        if (num[i] > max) {
            max = num[i];
        }
    }
    printf ("The minimum number is %g\n",min);
    printf ("The maximum number is %g\n",max);
}
