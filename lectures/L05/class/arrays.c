#include <stdio.h>

#define N 4

int main () {
    int a[N] = { 4, 3, 2, 1 };
    int b[N] = { 1, 2, 3, 4 };
    int c[N];
    int d[N];

    //    printf ("a[%d] = %d\n",0,a[0]);

    b[N] = 10;

    printf ("a = ");
    for (int i=0; i<N; i++) {
        printf ("%d ",a[i]);
    }
    printf ("\n");

    for (int i=0; i<N; i++) {
        c[i] = a[i];
    }

    printf ("c = ");
    for (int i=0; i<N; i++) {
        printf ("%d ",c[i]);
    }
    printf ("\n");

}
