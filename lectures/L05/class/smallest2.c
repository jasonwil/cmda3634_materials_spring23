#include <stdio.h>
#include <float.h>

#define N 10

int main () {
    float num[N] = { -2.3, -6.2, -4.5, 1.6, 2.3, 
        -0.7, 5.3, 0.9, 3.4, -6.2 };
    float min = FLT_MAX;
    float next = FLT_MAX;
    for (int i=0;i<N;i++) {
        if (num[i] < min) {
            next = min;
            min = num[i]; 
        }
        if ((num[i] > min) && (num[i] < next)) {
            next = num[i];
        }
    }
    printf ("The smallest number is %g\n",min);
    printf ("The next smallest number is %g\n",next);
}
