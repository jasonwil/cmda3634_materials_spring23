#include <stdio.h>

#define N 10

int main () {
    float num[N] = { -2.3, 7.2, -4.5, 1.6, 2.3, 
        -0.7, 5.3, 0.9, 3.4, 6.1 };
    printf ("num = ");
    for (int i=0;i<N;i++) {
        printf ("%g ",num[i]);
    }
    printf ("\n");
}
