#include <stdio.h>
#include <stdlib.h>

typedef unsigned int uint32;
typedef unsigned long int uint64;

/* compute the total stopping time for a given number n */
uint32 total_stopping_time (uint64 a_i) {
    uint32 total = 0;
    while (a_i != 1) {
        if (a_i % 2 == 0) {
            a_i = a_i/2;
        } else {
            a_i = (3*a_i+1);
        }
        total += 1;
    }
    return total;
}

int main(int argc, char **argv) {

    /* get N from command line */
    if (argc != 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    uint64 N = (uint64)atol(argv[1]);

    uint64 max_start = 1;
    uint32 max_total = 0;
    for (uint64 n = 1;n<=N;n++) {
        uint32 total = total_stopping_time(n);
        if (total > max_total) {
            max_start = n;
            max_total = total;
        }
    }

    /* output the results */
    printf ("The starting value less than or equal to %lu\n",N);
    printf ("  having the largest total stopping time is %lu\n",max_start);
    printf ("  which has %u steps\n",max_total);
}


