#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef unsigned long long int uint64;

int main (int argc, char** argv) {

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    uint64 N = atol(argv[1]);

    /* compute the mean */
    uint64 sum = 0;
    for (uint64 i=1;i<=N;i++) {
        sum += i;
    }
    double mean = (1.0)*sum/N;

    /* compute the sum of differences squared */
    /* compute the sample variance */
    double sum_diff_sq = 0;
    for (uint64 i=1;i<=N;i++) {
        sum_diff_sq += (i-mean)*(i-mean);
    }

    /* print the sample standard deviation */
    printf ("Sample standard deviation is %g\n",sqrt(sum_diff_sq/(N-1)));
}
