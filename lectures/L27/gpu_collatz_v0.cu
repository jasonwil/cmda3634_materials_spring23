#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>

typedef unsigned int uint32;
typedef unsigned long long int uint64;

/* compute the total stopping time for a given number n */
__device__ uint32 total_stopping_time (uint64 a_i) {
    uint32 total = 0;
    while (a_i != 1) {
        if (a_i % 2 == 0) {
            a_i = a_i/2;
        } else {
            a_i = (3*a_i+1);
        }
        total += 1;
    }
    return total;
}

__global__ void collatzKernel(uint64 N) {

    __shared__ uint64 max_start;
    __shared__ uint32 max_total;

    int thread_num = threadIdx.x;
    int num_threads = blockDim.x;

    if (thread_num == 0) {
        max_start = 1;
        max_total = 0;
    }
    __syncthreads();

    uint64 thread_max_start = 1;
    uint32 thread_max_total = 0;
    for (uint64 n = 1+thread_num;n<=N;n+=num_threads) {
        uint32 total = total_stopping_time(n);
        if (total > thread_max_total) {
            thread_max_start = n;
            thread_max_total = total;
        }
    }
    atomicMax(&max_total,thread_max_total);
    __syncthreads();
    if (max_total == thread_max_total) {
        atomicExch(&max_start,thread_max_start);
    }
    __syncthreads();

    if (thread_num == 0) {
        /* output the results */
        printf ("The starting value less than or equal to %lu\n",N);
        printf ("  having the largest total stopping time is %lu\n",max_start);
        printf ("  which has %u steps\n",max_total);
    }
}

int main (int argc, char** argv) {

    /* get N and num_threads from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }

    uint64 N = atol(argv[1]);
    int num_threads = atoi(argv[2]);

    printf ("num_threads = %d\n",num_threads); 
    collatzKernel <<< 1, num_threads >>> (N);
    cudaDeviceSynchronize();

}
