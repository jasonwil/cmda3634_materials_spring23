#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p p100_normal_q
#SBATCH -t 00:10:00
#SBATCH --gres=gpu:1
#SBATCH -o gpu_collatz.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for CUDA
module load cuda11.6/toolkit/11.6.2

# Build the executable
nvcc -arch=sm_60 -o gpu_collatz gpu_collatz.cu

# run gpu_collatz
time ./gpu_collatz $1 $2
