#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>

typedef unsigned long long int uint64;

__global__ void stdevKernel(uint64 N) {

    __shared__ uint64 sum;
    __shared__ double sum_diff_sq;

    int thread_num = threadIdx.x;
    int num_threads = blockDim.x;

    /* initialize sum and sum_diff_sq to 0 */
    if (thread_num == 0) {
        sum = 0;
        sum_diff_sq = 0;
    }
    __syncthreads();

    /* compute the mean */
    uint64 thread_sum = 0;
    for (uint64 i=1+thread_num;i<=N;i+=num_threads) {
        thread_sum += i;
    }
    atomicAdd(&sum,thread_sum);
    __syncthreads();
    double mean = (1.0)*sum/N;

    /* compute the sum of differences squared */
    double thread_sum_diff_sq = 0;
    for (uint64 i=1+thread_num;i<=N;i+=num_threads) {
        thread_sum_diff_sq += (i-mean)*(i-mean);
    }
    atomicAdd(&sum_diff_sq,thread_sum_diff_sq);
    __syncthreads();

    /* thread 0 prints the sample standard deviation */
    if (thread_num == 0) {
        printf (" Sample standard deviation is %g\n",sqrt(sum_diff_sq/(N-1)));
    }
}

int main (int argc, char** argv) {

    /* get N and num_threads from the command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
        return 1;
    }

    uint64 N = atol(argv[1]);
    int num_threads = atoi(argv[2]);

    printf ("num_threads = %d\n",num_threads); 
    stdevKernel <<< 1, num_threads >>> (N);
    cudaDeviceSynchronize();

}
