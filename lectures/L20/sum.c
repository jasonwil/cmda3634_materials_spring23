#include <stdio.h>
#include <stdlib.h>

typedef long long int int64;

int main(int argc, char** argv) {

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int64 N = atol(argv[1]);

    /* sum up numbers from 1 to N */
    long int sum = 0;
    for (long int i=1;i<=N;i++) {
        sum += i;
    }

    /* output results */
    printf ("sum = %ld, N*(N+1)/2 = %ld\n",sum,(N/2)*(N+1));

}
