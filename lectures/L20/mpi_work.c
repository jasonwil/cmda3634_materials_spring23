#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    long int sum = 0;
    long int N = 1000000000;
    for (int iter = 0;iter<1;iter++) {
	sum = 0;
        for (long int i=1;i<=N;i++) {
            sum += i;
        }
    }

    printf ("Sum from MPI rank %d of %d is %ld, N*(N+1)/2 is %ld\n",
            rank,size,sum,(N/2)*(N+1));

    MPI_Finalize();

    return 0;
}
