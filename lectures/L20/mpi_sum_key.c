#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

typedef long long int int64;

int main(int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }

    int64 N = atol(argv[1]);

    /* start the timer */
    double start_time, end_time;
    start_time = MPI_Wtime();

    long int sum = 0;
    for (long int i=1+rank;i<=N;i+=size) {
        sum += i;
    }

    printf (" sum from MPI rank %d of %d is %ld, N*(N+1)/2 is %ld\n",
            rank,size,sum,(N/2)*(N+1));

    int64 in_msg;
    int64 out_msg;
    MPI_Status status;
    if (rank==0) {
        for (int source=1;source<size;source++) {
            MPI_Recv(&in_msg,1,MPI_LONG_LONG_INT,source,0,MPI_COMM_WORLD,&status);
            sum += in_msg;
        }
    } else {
        out_msg = sum;
	int dest = 0;
        MPI_Send(&out_msg,1,MPI_LONG_LONG_INT,dest,0,MPI_COMM_WORLD);
    }

    /* stop the timer */
    end_time = MPI_Wtime();

    if (rank==0) {
        printf ("final sum = %ld, N*(N+1)/2 = %ld\n",sum,(N/2)*(N+1));
	printf ("elapsed time = %g seconds\n",end_time-start_time);
    }

    MPI_Finalize();
}
