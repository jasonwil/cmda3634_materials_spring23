#ifndef MAT_H
#define MAT_H

#include "vec.h"

typedef struct mat_s {
    double* data;
    int rows;
    int cols;
} mat_type;

/* initialize a matrix to use a given data buffer of size rows*cols */
/* mat_init must be called before any other matrix operation */
void mat_init (mat_type* A, double* data, int rows, int cols);

/* print a matrix using the provided format string */
void mat_print (mat_type* A, char* format);

/* C = A + B */
void mat_add (mat_type* A, mat_type* B, mat_type* C);

/* B = cA */
void mat_scalar_mult (mat_type* A, double c, mat_type* B);

/* reads a matrix from STDIN */
/* returns how many elements read */
int mat_read_stdin (mat_type* A);

/* returns || A - B ||^2_F */
/* The Frobenius norm squared of a matrix is the */
/* sum of the squares of all entries */
double mat_dist_sq (mat_type* A, mat_type* B);

/* performs the deep copy A->data[i] = B->data[i] for all i */
void mat_copy (mat_type* A, mat_type* B);

/* zeros the matrix A */
void mat_zero (mat_type* A);

/* initialize the vector row to the i^th row vector of A */
void mat_get_row (mat_type* A, vec_type* row, int i);

/* read a matrix from a binary file of unsigned chars */
void matrix_read_bin (mat_type* A, char* filename, int header_size);

#endif
