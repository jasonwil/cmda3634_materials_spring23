#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    /* get N from command line */
    if (argc != 2) {
	printf ("Command usage : %s %s\n",argv[0],"name");
	return 1;
    }

    char* name = argv[1];

    printf ("Hello World from %s!\n",name);

    return 0;
}
