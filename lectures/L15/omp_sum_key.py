import sys
import numpy as np
import matplotlib.pyplot as plt

# Load our C functions for computing sum using OpenMP
from ctypes import c_int, c_ulonglong, c_double, c_void_p, cdll
lib = cdll.LoadLibrary("./omp_sum.so")
omp_sum = lib.omp_sum
omp_sum.restype = c_double

# Read in the size of the sum N and the study size k
N = int(sys.argv[1]) 
k = int(sys.argv[2])

# Perform strong scaling study 
times = np.zeros(k)
result = np.zeros(2,dtype='ulonglong');
thread_count = 1
for i in range(k):
    print ('calling omp_sum with thread_count =',thread_count)
    times[i] = omp_sum (c_ulonglong(N),
                        c_void_p(result.ctypes.data),
                        c_int(thread_count))
    sum = int(result[0])+(int(result[1])<<64)
    print (' N=',N,', sum=',sum,', (N/2)*(N+1)=',int(N/2)*(N+1))
    print (' Elapsed Time=',round(times[i],5),'s')
    thread_count *= 2

# Plot results 
if (len(sys.argv) > 3):
    speedup = times[0]/times
    plt.rcParams['figure.figsize'] = (7, 7)
    plt.rcParams.update({'font.size': 14})
    plt.gca().set_aspect('equal')
    plt.scatter (range(0,k),np.log2(speedup),
                 color='maroon',s=100,label='N='+str(N))
    plt.plot ([0,k-1],[0,k-1],'--',color='orange',label='Ideal')
    plt.title ('OpenMP Sum Strong Scaling Study')
    plt.xlabel ('Log$_2$ Cores')
    plt.ylabel ('Log$_2$ Speedup')
    plt.legend();
    plt.savefig(sys.argv[3])
