#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long long int uint64;
typedef unsigned __int128 uint128;

double omp_sum (uint64 N, uint64* result, int thread_count) {
    uint128 sum = 0;

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

#pragma omp parallel num_threads(thread_count)
    {
	uint128 thread_sum = 0;
#pragma omp for
	for (uint64 i = 1; i <= N; i++) {
	    thread_sum += i;
	}
#pragma omp critical
	{
	    sum += thread_sum;
	}
    }

    /* stop the timer */
    end_time = omp_get_wtime();

    result[0] = (uint64)sum;
    result[1] = (uint64)(sum >> 64);
    return end_time-start_time;
}
