#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "mat.h"

typedef struct pair_s {
    double dist_sq;
    int i,j;
} pair_type;

pair_type find_extreme_pair (mat_type* A, int thread_count) {
    pair_type extreme_pair = { 0, -1, -1 };
#pragma omp parallel num_threads(thread_count)
    {
        pair_type thread_extreme_pair = { 0, -1, -1 };
        vec_type row1, row2;
#pragma omp for schedule(dynamic)
        for (int i = 0;i<A->rows-1;i++) {
            for (int j = i+1;j<A->rows;j++) {
                mat_get_row(A,&row1,i);
                mat_get_row(A,&row2,j);
                double dist_sq = vec_dist_sq(&row1,&row2);
                if (dist_sq > thread_extreme_pair.dist_sq) {
                    thread_extreme_pair.dist_sq = dist_sq;
                    thread_extreme_pair.i = i;
                    thread_extreme_pair.j = j;
                }
            }
        }
#pragma omp critical
        {
            if (thread_extreme_pair.dist_sq > extreme_pair.dist_sq) {
                extreme_pair = thread_extreme_pair;
            }
        }
    }
    return extreme_pair;
}

int main (int argc, char** argv) {

    /* get thread_count from command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"thread_count");
        return 1;
    }
    int thread_count = atoi(argv[1]);

    /* read the shape of the matrix from STDIN */
    int rows, cols;
    if (scanf("%d %d",&rows,&cols) != 2) {
        printf ("error reading the shape of the matrix\n");
        return 1;
    }

    /* dynamically allocate memory for the (rows x cols) matrix */
    double* A_data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix */
    mat_type A;
    mat_init (&A,A_data,rows,cols);

    /* read the matrix from STDIN */
    int num_read = mat_read_stdin (&A);

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* find the extreme pair */
    pair_type extreme_pair = find_extreme_pair (&A,thread_count);

    /* stop the timer */
    end_time = omp_get_wtime();

    /* output the results */
#ifdef TIMING
    printf ("(%d,%.4f),",thread_count,end_time-start_time);
#else
    printf ("thread_count = %d, ",thread_count);
    printf ("elapsed time = %g\n",end_time-start_time);
    printf (" Extreme Distance : %g,",sqrt(extreme_pair.dist_sq));
    printf (" Extreme Indices : %d %d\n",extreme_pair.i,extreme_pair.j);
#endif

    /* free the dynamically allocated matrix data buffer */
    free (A_data);

    return 0;
}
