#include <stdio.h>
#include <stdlib.h>

typedef long long int int64;

/* compute the total stopping time of a given start value */
int total_stopping_time (int64 start) {
    int64 total = 0;
    int64 a_i = start;
    while (a_i != 1) {
        total += 1;
        if (a_i % 2 == 0) {
            a_i /= 2;
        } else {
            a_i = 3*a_i + 1;
        }
    }
    return total;
}

int main (int argc, char** argv) {

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int64 N = atol(argv[1]);

    /* compute the start less than or equal to N with largest total stop */
    int64 max_start = 1;
    int64 max_total = 0;
    for (int64 start=1; start <= N; start++) {
        int64 total = total_stopping_time (start);
        if (total > max_total) {
            max_total = total;
            max_start = start;
        }
    }

    /* print results */
    printf ("The starting value less than or equal to %d\n",N);
    printf ("  having the largest total stopping time is %d\n",max_start);
    printf ("  which has %d steps\n",max_total);
}
