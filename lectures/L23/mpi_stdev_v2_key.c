#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

typedef long long int int64;

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int64 N = atol(argv[1]);

    /* start the timer */
    double start_time;
    start_time = MPI_Wtime();

    /* compute the mean */
    double sum = 0;
    for (int64 i=1+rank;i<=N;i+=size) {
        sum += i;
    }
    double in_msg;
    double out_msg;
    MPI_Status status;
    int source, dest;

    double total_sum;
    MPI_Allreduce(&sum,&total_sum,1,MPI_DOUBLE,
            MPI_SUM,MPI_COMM_WORLD);
    double mean = total_sum/N;

    /* compute the sample variance */
    double sum_diff_sq = 0;
    for (int64 i=1+rank;i<=N;i+=size) {
        sum_diff_sq += (i-mean)*(i-mean);
    }

    double total_sum_diff_sq;
    MPI_Reduce(&sum_diff_sq,&total_sum_diff_sq,1,MPI_DOUBLE,
            MPI_SUM,0,MPI_COMM_WORLD);
    double sample_variance = total_sum_diff_sq/(N-1);

    /* stop the timer */
    double end_time;
    end_time = MPI_Wtime();

    /* print the sample standard deviation */
    if (rank == 0) {
        double sample_stdev = sqrt(sample_variance);
        printf ("The sample standard deviation is %g\n",sample_stdev);
        printf ("elapsed time = %g seconds\n",end_time-start_time);
    }

    MPI_Finalize();
}
