#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

typedef long long int int64;

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int64 N = atol(argv[1]);

    /* start the timer */
    double start_time;
    start_time = MPI_Wtime();

    /* compute the mean */
    double sum = 0;
    for (int64 i=1+rank;i<=N;i+=size) {
        sum += i;
    }
    double in_msg;
    double out_msg;
    MPI_Status status;
    int source, dest;

    /* rank 0 sums up partial sums from each rank */
    /* assume size = 2^k for some integer k >= 0 */
    int alive = size;
    while (alive > 1) {
        if (rank < alive/2) {
            /* rank is a receiver */
            source = rank + alive/2;
            MPI_Recv (&in_msg, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD,&status);
            sum += in_msg;
        } else if (rank < alive) {
            /* rank is a sender */
            dest = rank - alive/2;
            out_msg = sum;
            MPI_Send (&out_msg, 1, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
        }
        alive = alive/2;
    }

    /* rank 0 broadcasts the total sum to all other ranks */
    /* assume size = 2^k for some integer k >= 0 */
    alive = 1;
    while (alive < size) {
        alive = alive*2;
        if (rank < alive/2) {
            /* rank is a sender */
            dest = rank + alive/2;
            out_msg = sum;
            MPI_Send(&out_msg,1,MPI_DOUBLE,dest,0,MPI_COMM_WORLD);
        } else if (rank < alive) {
            /* rank is a receiver */
            source = rank - alive/2;
            MPI_Recv(&in_msg,1,MPI_DOUBLE,source,0,
                    MPI_COMM_WORLD,&status);
            sum = in_msg;
        }
    }
    double mean = sum/N;

    /* compute the sample variance */
    double sum_diff_sq = 0;
    for (int64 i=1+rank;i<=N;i+=size) {
        sum_diff_sq += (i-mean)*(i-mean);
    }

    /* rank 0 sums up partial sums from each rank */
    /* assume size = 2^k for some integer k >= 0 */
    alive = size;
    while (alive > 1) {
        if (rank < alive/2) {
            /* rank is a receiver */
            source = rank + alive/2;
            MPI_Recv (&in_msg, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD,&status);
            sum_diff_sq += in_msg;
        } else if (rank < alive) {
            /* rank is a sender */
            dest = rank - alive/2;
            out_msg = sum_diff_sq;
            MPI_Send (&out_msg, 1, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD);
        }
        alive = alive/2;
    }
    double sample_variance = sum_diff_sq/(N-1);

    /* stop the timer */
    double end_time;
    end_time = MPI_Wtime();

    /* print the sample standard deviation */
    if (rank == 0) {
        double sample_stdev = sqrt(sample_variance);
        printf ("The sample standard deviation is %g\n",sample_stdev);
        printf ("elapsed time = %g seconds\n",end_time-start_time);
    }

    MPI_Finalize();
}
