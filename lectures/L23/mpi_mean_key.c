#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "mat.h"

int main (int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get filename from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"filename");
        return 1;
    }

    /* try to open the file */
    FILE* fptr;
    fptr = fopen(argv[1],"r"); 

    /* need to check for null */
    if (fptr == 0) {
        printf ("Error opening data file.\n");
        exit(1);
    }

    /* read the shape of the matrix from the file */
    int rows, cols;
    if (fscanf(fptr,"%d %d",&rows,&cols) != 2) {
        printf ("error reading the shape of the matrix\n");
        return 1;
    }

    /* dynamically allocate memory for the (rows x cols) matrix */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* initialize the matrix */
    mat_type dataset;
    mat_init (&dataset,data,rows,cols);

    /* read the matrix from STDIN */
    int num_read = mat_read_file (&dataset,fptr);

    /* close the file */
    fclose(fptr);

    /* start the timer */
    double start_time;
    start_time = MPI_Wtime();

    /* set elements of the mean vector to 0 */
    vec_type mean;
    double mean_data[cols];
    vec_init (&mean,mean_data,cols);
    vec_zero(&mean);

    /* compute the sum of the data */
    for (int i=0+rank;i<dataset.rows;i+=size) {
        vec_type next;
        mat_get_row(&dataset,&next,i);
        vec_add(&mean,&next,&mean);
    }

    /* reduce the sum to rank 0 and then broadcast */
    MPI_Allreduce(MPI_IN_PLACE,mean_data,cols,MPI_DOUBLE,
            MPI_SUM,MPI_COMM_WORLD);

    /* divide by the number of rows */
    vec_scalar_mult(&mean,1.0/dataset.rows,&mean);

    /* stop the timer */
    double end_time;
    end_time = MPI_Wtime();

    if (rank == 0) {
        printf ("mean = ");
        vec_print (&mean,"%.1lf ");
        printf ("elapsed time = %g seconds\n",end_time-start_time);
    }

    /* free the dynamically allocated data buffer */
    free (data);

    MPI_Finalize();
}
