#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

typedef long long int int64;

int main(int argc, char** argv) {

    MPI_Init (&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);

    /* get N from the command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"N");
        return 1;
    }
    int64 N = atol(argv[1]);

    /* start the timer */
    double start_time;
    start_time = MPI_Wtime();

    /* sum up numbers from 1 to N */
    long int sum = 0;
    for (long int i=1+rank;i<=N;i+=size) {
        sum += i;
    }

    int64 in_msg;
    int64 out_msg;
    MPI_Status status;
    int source, dest;

    int64 total_sum;
    MPI_Reduce(&sum,&total_sum,1,MPI_LONG_LONG_INT,
            MPI_SUM,0,MPI_COMM_WORLD);

    /* stop the timer */
    double end_time;
    end_time = MPI_Wtime();

    /* output results */
    if (rank == 0) {
        printf ("total sum = %ld, N*(N+1)/2 = %ld\n",total_sum,(N/2)*(N+1));
        printf ("elapsed time = %g seconds\n",end_time-start_time);
    }

    MPI_Finalize();
}
