#!/bin/bash
#SBATCH -t 10
#SBATCH -p normal_q
#SBATCH -A cmda3634_rjh
#SBATCH --nodes=16
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=128
#SBATCH -o collatz_omp_mpi.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need
module load matplotlib

# Compile the C shared object file
gcc -O3 -fPIC -shared -o collatz_omp_mpi.so collatz_omp_mpi.c -fopenmp

# run the collatz Python script
time mpiexec -np $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node:PE=$SLURM_CPUS_PER_TASK -x OMP_PROC_BIND=true python3 collatz_omp_mpi.py $1
