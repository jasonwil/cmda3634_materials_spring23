import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 3):
    print ("Command Usage : python3",sys.argv[0],"datafile","k")
    exit(1)
datafile = sys.argv[1]
k = int(sys.argv[2])

# read the data file
data = np.loadtxt(datafile,skiprows=1)

# farfirst algorithm
centers = np.zeros(k+1,dtype='int')
centers[0] = 0
dist_sq = np.ones((len(data),2))*np.inf
for i in range(k):
    dist_sq[:,1] = np.sum((data-data[centers[i]])*(data-data[centers[i]]),axis=1)
    min_dist_sq = np.min(dist_sq,axis=1)
    cost_sq = np.max(min_dist_sq)
    centers[i+1] = np.argmax(min_dist_sq)
    np.copyto(dist_sq[:,0],min_dist_sq)

# print results
print ('# approximate optimal cost =',np.round(np.sqrt(cost_sq),5))
print ('# approx optimal centers :')
print (data[centers[:k]])
