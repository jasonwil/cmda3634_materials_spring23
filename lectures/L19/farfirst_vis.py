import sys
import numpy as np
import matplotlib.pyplot as plt

# the name of the output file is a command line argument
if (len(sys.argv) < 3):
    print ("Command Usage : python3",sys.argv[0],"datafile","k")
    exit(1)
datafile = sys.argv[1]
k = int(sys.argv[2])
imagefile = datafile.split('.',1)[0]

# read the data file
data = np.loadtxt(datafile,skiprows=1)

# plot the data
plt.gca().set_aspect('equal')
plt.scatter(data[:,0],data[:,1],s=1,color='blue')
plt.savefig(imagefile+'_'+str(0).zfill(4)+'.png')

# visualize farfirst algorithm
centers = np.zeros(k,dtype='int')
centers[0] = 0
plt.scatter(data[centers[0],0], data[centers[0],1], c='red', s=100, alpha=0.8)
plt.savefig(imagefile+'_'+str(1).zfill(4)+'.png')
dist_sq = np.ones((len(data),2))*np.inf
for i in range(k-1):
    dist_sq[:,1] = np.sum((data-data[centers[i]])*(data-data[centers[i]]),axis=1)
    min_dist_sq = np.min(dist_sq,axis=1)
    centers[i+1] = np.argmax(min_dist_sq)
    np.copyto(dist_sq[:,0],min_dist_sq)
    plt.scatter(data[centers[i+1],0], data[centers[i+1],1], c='red', s=100,alpha=0.8)
    plt.savefig(imagefile+'_'+str(i+2).zfill(4)+'.png')
print (data[centers])


