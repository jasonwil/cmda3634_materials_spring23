#!/bin/bash
#SBATCH -t 10
#SBATCH -p normal_q
#SBATCH -A cmda3634_rjh
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH -o mpi_hello.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need
module load matplotlib

# run the hello workd Python script
mpiexec -np $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node python3 mpi_hello.py
