import sys
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if (rank == 0):
    in_msg = np.empty(5,dtype='int32')
    for i in range(1,size):
        comm.Recv (in_msg,source=i)
        print ('rank 0 received message',in_msg,'from rank',i)
else:
    np.random.seed(rank+int(sys.argv[1]))
    out_msg = np.random.randint(0,10,5,dtype='int32')
    comm.Send (out_msg,dest=0)
    
